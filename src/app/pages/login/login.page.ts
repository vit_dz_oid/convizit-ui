import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { FormService } from '../../services/form.service';
import { MessageService } from '../../services/message.service';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'cv-login-page',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],

})
export class LoginPage implements OnInit {
  loginForm = this.fb.group({
    username: [
      '',
      [
        (ctrl) => Validators.required(ctrl) ? { requiredUsername: true } : null,
        (ctrl) => this.formService.validators.emailPattern(ctrl) ? { usernameEmailPattern: true } : null
      ]
    ],
    password: [
      '',
      [
        (ctrl) => Validators.required(ctrl) ? { requiredPassword: true } : null
      ]
    ]
  });
  error = null;
  errorFields = [];

  constructor(
    private fb: FormBuilder,
    private formService: FormService,
    private messageService: MessageService,
    private routing: Router,
    private authService: AuthService,
  ) { }

  ngOnInit() {
  }

  submit() {
    this.error = null;

    if (this.loginForm.invalid) {
      this._updErrorField();
      this._updErrorMsg();
    } else {
      const { username, password } = this.loginForm.value;
      this.authService.login({ username, password })
        .then((data) => {
          this.routing.navigateByUrl('/index');
        })
        .catch(errMsg => {
          this.errorFields = ['username', 'password'];
          this.error = errMsg;
        })
    }
  }

  private _updErrorField() {
    this.errorFields = [];

    if (this.loginForm.get('username').invalid) {
      this.errorFields = ['username'];
    } else if (this.loginForm.get('password').invalid) {
      this.errorFields = ['password'];
    } else {
      this.errorFields = [];
    }
  }

  private _updErrorMsg() {
    const errors = [];

    this.formService.errorGrabber(this.loginForm, errors);
    const error = errors[0]!.name;

    this.error = this.messageService.getInputErrorMessage(error);
  }
}

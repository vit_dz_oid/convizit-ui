import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { FormService } from '../../services/form.service';
import { MessageService } from '../../services/message.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'cv-set-password',
  templateUrl: './set-password.component.html',
  styleUrls: ['./set-password.component.scss']
})
export class SetPasswordPage implements OnInit {

  userEmail: string;
  userId: number;
  token: string;
  errorFields: Array<string> = [];
  error: string;

  setForm: FormGroup = this.fb.group({
    password: [
      '',
      [
        (ctrl) => Validators.required(ctrl) ? { requiredPassword: true } : null
      ]
    ]
  })

  constructor(
    private fb: FormBuilder,
    private formService: FormService,
    private messageService: MessageService,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService
  ) { }

  ngOnInit(): void {

    this.route.params
      .subscribe(param => {
        const { token } = param;

        this.token = token;

        this.authService.checkResetToken({ token })
          .then(data => {

            const { userEmail, userId } = data;
            this.userEmail = userEmail;
            this.userId = userId;

          })
          .catch(error => {
            this.router.navigateByUrl('/auth');
          })
      })

  }

  submit(): void {

    this.error = null;

    if (this.setForm.invalid) {
      this._updErrorField();
      this._updErrorMsg();
    } else {
      const { password } = this.setForm.value;
      const token = this.token;

      this.authService.setPassword({ password, token })
        .then(() => {
          this.router.navigate(['/auth']);
        })
        .catch(errMsg => {
          this.errorFields = ['password'];
          this.error = errMsg;
        });
        
    }

  }

  private _updErrorField(): void {
    this.errorFields = [];

    if (this.setForm.get('password').invalid) {
      this.errorFields = ['password'];
    }
  }

  private _updErrorMsg(): void {
    const errors = [];

    this.formService.errorGrabber(this.setForm, errors);
    const error = errors[0]!.name;

    this.error = this.messageService.getInputErrorMessage(error);
  }

}

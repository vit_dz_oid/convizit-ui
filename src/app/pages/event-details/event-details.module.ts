import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EventDetailsRoutingModule } from './event-details-routing.module';
import { EventDetailsComponent } from './event-details.component';

@NgModule({
  imports: [
    CommonModule,
    EventDetailsRoutingModule
  ],
  declarations: [EventDetailsComponent]
})
export class EventDetailsModule { }

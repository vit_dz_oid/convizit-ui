import { Injectable } from '@angular/core';
import { DataService } from '../../services/data.service';

@Injectable()
export class BoardingService {

  constructor(
    private dataService: DataService
  ) { }

  checkStatus(checkData?: { token: string }): any {
    return new Promise((resolve, reject) => {
      this.dataService.api({
        type: 'isVerified',
        data: Object.assign({}, checkData)
      }).subscribe(
        res => {
          if (!res.error) {
            resolve(res);
          } else {
            reject(res.error);
          }
        },
        err => {
          reject(err);
        }
      )
    })
  }
}

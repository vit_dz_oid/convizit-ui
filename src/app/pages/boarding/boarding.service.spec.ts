import { TestBed, inject } from '@angular/core/testing';

import { BoardingService } from './boarding.service';

describe('BoardingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BoardingService]
    });
  });

  it('should be created', inject([BoardingService], (service: BoardingService) => {
    expect(service).toBeTruthy();
  }));
});

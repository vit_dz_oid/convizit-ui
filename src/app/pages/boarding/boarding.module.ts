import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BoardingRoutingModule } from './boarding-routing.module';
import { BoardingComponent } from './boarding.component';
import { SharedModule } from '../../modules/shared.module';
import { BoardingService } from './boarding.service';

@NgModule({
  imports: [
    CommonModule,
    BoardingRoutingModule,
    SharedModule
  ],
  providers: [
    BoardingService
  ],
  declarations: [BoardingComponent]
})
export class BoardingModule { }

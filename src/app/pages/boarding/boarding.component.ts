import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { FormService } from '../../services/form.service';
import { MessageService } from '../../services/message.service';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { ModalService } from '../../services/modal.service';

@Component({
  selector: 'cv-boarding',
  templateUrl: './boarding.component.html',
  styleUrls: ['./boarding.component.scss']
})
export class BoardingComponent implements OnInit {

  errorFields: Array<string> = [];
  error: string;
  verifyError: boolean = false;

  boardingForm = this.fb.group({
    email: [
      '',
      [
        (ctrl) => Validators.required(ctrl) ? { requiredEmail: true } : null,
        (ctrl) => this.formService.validators.emailPattern(ctrl) ? { usernameEmailPattern: true } : null
      ]
    ]
  })

  constructor(
    private fb: FormBuilder,
    private formService: FormService,
    private messageService: MessageService,
    private authService: AuthService,
    private router: Router,
    private modalService: ModalService,
  ) { }

  ngOnInit() {
  }

  submit(): void {
    this.errorFields = [];
    this.error = null;

    if (this.boardingForm.invalid) {
      this._updErrorField();
      this._updErrorMsg();
    } else {
      const { email } = this.boardingForm.value;
    }
  }

  verifyProject(): void {
    this.verifyError = false;
    this.authService.isLogged()
      .subscribe(
        res => {
          const { isVerified } = res;
          if (isVerified) {
            this.router.navigateByUrl('');
          } else {
            this.verifyError = true;
          }
        },
        errMsg => {
          console.log(errMsg);
        }
      )
  }

  openInstractions(): void {
    this.modalService.set({
      type: 'installCode'
    }).subscribe();
  }

  private _updErrorField(): void {
    this.errorFields = [];

    if (this.boardingForm.get('email').invalid) {
      this.errorFields = ['email'];
    } else {
      this.errorFields = [];
    }
  }

  private _updErrorMsg(): void {
    const errors = [];

    this.formService.errorGrabber(this.boardingForm, errors);
    const error = errors[0]!.name;

    this.error = this.messageService.getInputErrorMessage(error);
  }

}

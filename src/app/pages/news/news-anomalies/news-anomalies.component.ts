import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NewsService } from '../news-service.service';

@Component({
  selector: 'cv-news-anomalies',
  templateUrl: './news-anomalies.component.html',
  styleUrls: ['./news-anomalies.component.scss']
})
export class NewsAnomaliesComponent {
  filterType: 'all'|'pinned' = 'all';

  get anomalies() {
    this.filterType = this.route.snapshot.data.type;
    return this.newsService.anomalies.filter(item => {
      if (this.filterType === 'all') return true;

      return !!item.isPinned;
    });
  }

  constructor(
    protected newsService: NewsService,
    private route: ActivatedRoute
  ) { }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsAnomaliesComponent } from './news-anomalies.component';

describe('NewsAnomaliesComponent', () => {
  let component: NewsAnomaliesComponent;
  let fixture: ComponentFixture<NewsAnomaliesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsAnomaliesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsAnomaliesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

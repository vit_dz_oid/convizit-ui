import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsConversionComponent } from './news-conversion.component';

describe('NewsConversionComponent', () => {
  let component: NewsConversionComponent;
  let fixture: ComponentFixture<NewsConversionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsConversionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsConversionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

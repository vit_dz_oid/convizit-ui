import { Component, OnInit, Input } from '@angular/core';
import { NewsService } from '../news-service.service';

@Component({
  selector: 'cv-news-conversion',
  templateUrl: './news-conversion.component.html',
  styleUrls: ['./news-conversion.component.scss']
})
export class NewsConversionComponent {
  get conversions() {
    return this.newsService.topElementChanges;
  }

  get changes() {
    return this.newsService.change !== null;
  }

  get loading() {
    return this.newsService.loading || !this.newsService.topElementChanges;
  }

  get typeOfView() {
    if (this.loading) { //loading view
      return 1;
    } else if((this.newsService.change === null) && !this.conversions.length) { // no setted changes
      return 2;
    } else if (this.newsService.change === null) { // no changes
      return 3;
    }

    return 4; // all ok
  }

  constructor(private newsService: NewsService) {}
}

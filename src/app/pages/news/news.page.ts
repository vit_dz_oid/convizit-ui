import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ModalService } from '../../services/modal.service';
import { NewsService } from './news-service.service';

@Component({
  selector: 'cv-news',
  templateUrl: './news.page.html',
  styleUrls: ['./news.page.scss']
})
export class NewsPage implements OnInit {
  selected = '';
  types = [
    {
      label: 'All events',
      id: 'all'
    },
    {
      label: 'Conversions',
      id: 'conversions'
    },
    {
      label: 'Sessions',
      id: 'session'
    },
    {
      label: 'Clicks',
      id: 'click'
    },
  ];
  
  get emptyAnomalies() {
    const { change, anomalies, loading } = this.newsService;
    return !loading && (change === null) && !!anomalies;
  }

  constructor(
    public newsService: NewsService
  ) { }

  ngOnInit(): void {   
    this.newsService.updData();
    this.selected = this.types[0].label;
  }

  search(val: string) {
    if (typeof val  != 'string') return;

    this.newsService.toSearch(val);
  }

  selectType(type: string) {
    this.selected = this.types.find(el => el.id === type).label;
    this.newsService.updFilter(type);
  }
}

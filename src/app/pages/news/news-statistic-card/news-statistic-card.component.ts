import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'cv-news-statistic-card',
  templateUrl: './news-statistic-card.component.html',
  styleUrls: ['./news-statistic-card.component.scss']
})
export class NewsStatisticCardComponent {
  @Input() conversionRate = 0;
  @Input() total = 0;
  @Input() change = 0;
}

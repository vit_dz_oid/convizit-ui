import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsStatisticCardComponent } from './news-statistic-card.component';

describe('NewsStatisticCardComponent', () => {
  let component: NewsStatisticCardComponent;
  let fixture: ComponentFixture<NewsStatisticCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsStatisticCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsStatisticCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

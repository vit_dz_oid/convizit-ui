import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsPage } from './news.page';
import { NewsAnomaliesComponent } from './news-anomalies/news-anomalies.component';

const routes: Routes = [
  {
    path: '',
    component: NewsPage,
    children: [
      {
        path: 'all',
        component: NewsAnomaliesComponent,
        data: { type: 'all'}
      },
      {
        path: 'pinned',
        component: NewsAnomaliesComponent,
        data: { type: 'pinned'}
      },
      { path: '**', redirectTo: 'all'},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewsRoutingModule { }

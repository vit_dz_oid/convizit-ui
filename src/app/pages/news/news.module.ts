import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsPage } from './news.page';
import { NewsRoutingModule } from './news-routing.module';
import { SharedModule } from '../../modules/shared.module';
import { NewsService } from './news-service.service';
import { NewsReferrerComponent } from './news-referrer/news-referrer.component';
import { NewsConversionComponent } from './news-conversion/news-conversion.component';
import { NewsStatisticCardComponent } from './news-statistic-card/news-statistic-card.component';
import { NewsAnomaliesComponent } from './news-anomalies/news-anomalies.component';
import { NewsAnomalyComponent } from './news-anomaly/news-anomaly.component';
import { WorkstageModule } from '../../modules/workstage/workstage.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    NewsRoutingModule,
    WorkstageModule,
  ],
  declarations: [
    NewsPage,
    NewsConversionComponent,
    NewsReferrerComponent,
    NewsStatisticCardComponent,
    NewsAnomaliesComponent,
    NewsAnomalyComponent,
  ],
  providers: [
    NewsService
  ]
})
export class NewsModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsAnomalyComponent } from './news-anomaly.component';

describe('NewsAnomalyComponent', () => {
  let component: NewsAnomalyComponent;
  let fixture: ComponentFixture<NewsAnomalyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsAnomalyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsAnomalyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'cv-news-anomaly',
  templateUrl: './news-anomaly.component.html',
  styleUrls: ['./news-anomaly.component.scss']
})
export class NewsAnomalyComponent {
  @Input() changes = 0;
  @Input() lastCompare = 0;
  @Input() timestamp = 0;
  @Input() anomalyType = '';
  @Input() name = null;
  @Input() loading = false;
  @Input() search = '';

  get timeAgo() {
    const lc = this.lastCompare;

    if (lc >= 2592000) {
      return 'months';
    } else if (lc >= 604800) {
      return 'weeks'
    } else if (lc >= 86400) {
      return 'days'
    } else if (lc >= 3600) {
      return 'hours'
    } else {
      return 'minutes'
    }
  }

  get locAnomalyType() {
    let result = '';

    if (this.name) {
      result = this.name;
    } else {
      const anomalies = {
        click: 'Clicks',
        session: 'Sessions'
      };
  
      result = anomalies[this.anomalyType];
    }
    // TODO: fix this shit
    return result.replace(new RegExp(this.search, 'gi'), '<span class="highlight">$&</span>');
  }

  get anomalyTitle() {
    const anomalyTitles = {
      click: 'Clicks on',
      session: 'Number of'
    };

    return anomalyTitles[this.anomalyType];
  }
}

import { Injectable } from '@angular/core';
import { tap, delay } from 'rxjs/operators';
import { DataService } from '../../services/data.service';
import { StatisticAnomaly, Anomaly } from '../../interfaces/server.interface';
import { WorkstageService } from '../../modules/workstage/workstage.service';

@Injectable()
export class NewsService {
  change = null;
  conversionRate = null;
  topElementChanges = null;
  topReferrerChanges = null;
  total = null;
  unique = null;
  anomalies: Array<Anomaly> = null;
  loading = false;
  type: 'click' | 'session' | 'conversion' | 'all' = 'all';
  
  get search() {
    return this._search;
  }

  private _period = '';
  private _anomalies = null;
  private _search = '';

  constructor(
    private dataService: DataService,
    private workstageService: WorkstageService
  ) {
    this._setSubscriptions();
  }

  updStatisticsData() {
    this.change = this.conversionRate = this.topElementChanges = this.topReferrerChanges = this.total = this.unique = null;

    return new Promise((resolve, reject) => {
      this.dataService.api({
        type: 'statisticAnomaly',
        queryParams: {
          range: this._period,
        }
      }).pipe(tap((res: StatisticAnomaly) => {
        this.change = res.change;
        this.conversionRate = res.conversionRate;
        this.topElementChanges = res.topElementChanges;
        this.topReferrerChanges = res.topReferrerChanges;
        this.total = res.total;
        this.unique = res.unique;
      })).subscribe(resolve);
    });
  }

  updAnomaly() {
    this.anomalies = null;

    return new Promise((resolve, reject) => {
      this.dataService.api({
        type: 'anomaly',
        queryParams: {
          range: this._period, //from top select
          conversion: false,
          pinned: false,
        }
      }).pipe(tap((res: Array<Anomaly>) => {
        this._anomalies = res;
        this.updFilter();
      })).subscribe(resolve);
    })
  }

  updData() {
    this.loading = true;
    return Promise.all([this.updAnomaly(), this.updStatisticsData()]).then(() => {
      this.loading = false;
    });
  }

  toSearch(val) {
    this._search = val;
    this.updFilter();
  }

  updFilter(type?) {
    type && (this.type = type);

    this.anomalies = this._anomalies
      .filter((i: Anomaly) => { // type filter
        if (this.type === 'all') return true;

        return i.type === this.type;
      })
      .filter((i: Anomaly) => { // search filter
        if (this.search) {
          const { type, element: { name } } = i; 
          return type.includes(this.search) || (name && name.includes(this.search));
        }

        return true;
      });
  }

  // Reaction on changing of period from header
  private _setSubscriptions() {
    if (!this._period) this._period = this.workstageService.period;

    this.workstageService.selectedPeriod$.subscribe((period) => {
      this._period = period;
      this.updData();
    });
  }
}

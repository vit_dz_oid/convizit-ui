import { Component, OnInit, Input } from '@angular/core';
import { NewsService } from '../news-service.service';

@Component({
  selector: 'cv-news-referrer',
  templateUrl: './news-referrer.component.html',
  styleUrls: ['./news-referrer.component.scss']
})
export class NewsReferrerComponent {
  get referrers() {
    return this.newsService.topReferrerChanges;
  }

  get changes() {
    return this.newsService.change !== null;
  }

  get loading() {
    return this.newsService.loading || !this.newsService.topReferrerChanges;
  }

  get typeOfView() {
    if (this.loading) { //loading view
      return 1;
    } else if(!this.referrers.length) { // no referrers
      return 2;
    }

    return 3; // all ok
  }

  constructor(private newsService: NewsService) {}
}

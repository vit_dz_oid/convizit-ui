import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsReferrerComponent } from './news-referrer.component';

describe('NewsReferrerComponent', () => {
  let component: NewsReferrerComponent;
  let fixture: ComponentFixture<NewsReferrerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsReferrerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsReferrerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

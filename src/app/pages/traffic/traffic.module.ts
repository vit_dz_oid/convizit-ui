import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrafficRoutingModule } from './traffic-routing.module';
import { TrafficComponent } from './traffic.component';
import { TrafficService } from './traffic.service';
import { SharedModule } from '../../modules/shared.module';
import { TrafficSourceComponent } from './traffic-sources/traffic-source.component';

@NgModule({
  imports: [
    CommonModule,
    TrafficRoutingModule,
    SharedModule
  ],
  providers: [
    TrafficService
  ],
  declarations: [TrafficComponent, TrafficSourceComponent]
})
export class TrafficModule { }

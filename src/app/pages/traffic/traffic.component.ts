import { Component, OnInit } from '@angular/core';
import { TrafficService } from './traffic.service';
import { TrafficSource } from '../../interfaces/server.interface';

@Component({
  selector: 'cv-traffic',
  templateUrl: './traffic.component.html',
  styleUrls: ['./traffic.component.scss']
})
export class TrafficComponent implements OnInit {

  constructor(
    private trafficService: TrafficService
  ) { }

  ngOnInit(): void {
    this.trafficService.updData();
  }

  get trafficSources(): Array<TrafficSource> {
    const { trafficSources } = this.trafficService;
    return trafficSources;
  }

  search(event): void {
    this.trafficService.updSearch(event);
  }

}

import { Injectable } from '@angular/core';
import { WorkstageService } from '../../modules/workstage/workstage.service';
import { DataService } from '../../services/data.service';
import { TrafficSource } from '../../interfaces/server.interface';
import { tap } from 'rxjs/internal/operators';

@Injectable()
export class TrafficService {

  private _period: string = '';
  private _trafficSources: Array<TrafficSource> = [];
  private _searchValue: string = '';

  loading: boolean = false;
  trafficSources: Array<TrafficSource> = [];

  constructor(
    private dataService: DataService,
    private workstageService: WorkstageService
  ) {
    this._setSubscriptions();
  }

  updData(): Promise<TrafficSource> {
    this.loading = true;
    return new Promise<TrafficSource>((resolve, reject) => {
      this.dataService.api({
        type: 'getTrafficSources',
        queryParams: {
          range: this._period
        }
      }).pipe(tap((res: Array<TrafficSource>) => {
        this._trafficSources = res;
        this.updFilter();
      })).subscribe();
    });
  }

  updSearch(searchValue: string) {
    this._searchValue = searchValue.toLowerCase();
    this.updFilter();
  }

  updFilter() {
    this.trafficSources = this._trafficSources
      .filter((item: TrafficSource) => {
        const { name } = item;
        return name.toLowerCase().includes(this._searchValue);
      });
  }

  private _setSubscriptions() {
    if (!this._period) {
      this._period = this.workstageService.period;
    }

    this.workstageService.selectedPeriod$.subscribe((period) => {
      this._period = period;
      this.updData();
    });
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrafficSourceComponent } from './traffic-source.component';

describe('TrafficSourcesComponent', () => {
  let component: TrafficSourceComponent;
  let fixture: ComponentFixture<TrafficSourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrafficSourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrafficSourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

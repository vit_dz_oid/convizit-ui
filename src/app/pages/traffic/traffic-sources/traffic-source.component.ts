import { Component, OnInit, Input } from '@angular/core';
import { TrafficSource } from '../../../interfaces/server.interface';

@Component({
  selector: 'cv-traffic-source',
  templateUrl: './traffic-source.component.html',
  styleUrls: ['./traffic-source.component.scss']
})
export class TrafficSourceComponent implements OnInit {

  @Input() source: TrafficSource;

  constructor() { }

  ngOnInit() {
  }

}

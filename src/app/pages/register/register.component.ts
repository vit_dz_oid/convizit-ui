import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { FormService } from '../../services/form.service';
import { MessageService } from '../../services/message.service';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'cv-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterPage implements OnInit {

  recaptchaToken: string;
  error: string;
  errorFields: Array<string> = [];
  showAdditionalInfo: boolean = false;
  sizes: Array<string> = ['1 - 19 employees', '20 - 50 employees', '51 - 100 employees', '101 - 500 employees', '501 - 1000 employees', '1001 - 5000 employees', '5001 - 10000 employees', '10000+ employees'];
  titles: Array<string> = ['Marketing', 'Product', 'Customer Success', 'Engineering', 'Other'];

  /**
   * Recaptcha params
   */
  theme: string = 'light';
  size: string = 'normal';
  lang: string = 'en';
  type: string = 'image';

  registerForm: FormGroup = this.fb.group({
    name: [
      '',
      [
        (ctrl) => Validators.required(ctrl) ? { requiredName: true } : null,
      ]
    ],
    username: [
      '',
      [
        (ctrl) => Validators.required(ctrl) ? { requiredEmail: true } : null,
        (ctrl) => this.formService.validators.emailPattern(ctrl) ? { emailPattern: true } : null
      ]
    ],
    password: [
      '',
      [
        (ctrl) => Validators.required(ctrl) ? { requiredPassword: true } : null
      ]
    ]
  });

  addRegisterForm: FormGroup = this.fb.group({
    company: [
      null
    ],
    size: [
      null
    ],
    title: [
      null
    ]
  })


  constructor(
    private fb: FormBuilder,
    private formService: FormService,
    private messageService: MessageService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  continue() {
    this.error = null;
    this.errorFields = [];

    if (this.registerForm.invalid) {
      this._updErrorField();
      this._updErrorMsg();
    } else {
      const { email } = this.registerForm.value;

      this.authService.checkEmailUsed({ email })
        .then(data => {
          const { status } = data;
          if (status) {
            this.errorFields = ['email'];
            this.error = 'User already exists';
          } else {
            this.showAdditionalInfo = true;
          }
        })
        .catch(errMsg => {
          this.error = errMsg;
        })
    }
  }

  submit() {
    if (!this.recaptchaToken) {
      this.error = 'Enter Captcha to continue';
    } else {
      const { name, username, password } = this.registerForm.value;
      const { company, size, title } = this.addRegisterForm.value;

      this.authService.registration({ name, username, password, company, size, title })
        .then(data => {
          const { isLogin, isVerified } = data;
          if (isLogin) {
            this.router.navigateByUrl('/index');
          }
        })
        .catch(errMsg => {
          console.log(errMsg);
        });
    }
  }

  handleSuccess(event) {
    this.recaptchaToken = event;
  }

  handleExpire(){
    this.recaptchaToken = null;
  }

  private _updErrorField() {
    this.errorFields = [];

    if (this.registerForm.get('name').invalid) {
      this.errorFields = ['name'];
    } else if (this.registerForm.get('username').invalid) {
      this.errorFields = ['email'];
    } else if (this.registerForm.get('password').invalid) {
      this.errorFields = ['password'];
    } else {
      this.errorFields = [];
    }
  }

  private _updErrorMsg() {
    const errors = [];

    this.formService.errorGrabber(this.registerForm, errors);
    const error = errors[0]!.name;

    this.error = this.messageService.getInputErrorMessage(error);
  }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { FormService } from '../../services/form.service';
import { MessageService } from '../../services/message.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'cv-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordPage implements OnInit {

  resetForm: FormGroup = this.fb.group({
    email: [
      '',
      [
        (ctrl) => Validators.required(ctrl) ? { requiredEmail: true } : null,
        (ctrl) => this.formService.validators.emailPattern(ctrl) ? { emailPattern: true } : null
      ]
    ]
  });

  errorFields: Array<string> = [];
  error: string;
  isSent: boolean = false;

  constructor(
    private fb: FormBuilder,
    private formService: FormService,
    private messageService: MessageService,
    private authService: AuthService
  ) { }

  ngOnInit() {
  }

  submit(): void {
    this.errorFields = [];
    this.error = null;

    if (this.resetForm.invalid) {
      this._updErrorField();
      this._updErrorMsg();
    } else {
      const { email } = this.resetForm.value;

      this.authService.resetPassword({ email })
        .then(() => {
          this.isSent = true;
        })
        .catch(errMsg => {
          this.errorFields = ['email'];
          this.error = errMsg;
        })
    }
  }

  clearForm(): void {
    this.resetForm.controls['email'].setValue('');
    this.isSent = false;
  }


  private _updErrorField(): void {
    this.errorFields = [];

    if (this.resetForm.get('email').invalid) {
      this.errorFields = ['email'];
    }
  }

  private _updErrorMsg(): void {
    const errors = [];

    this.formService.errorGrabber(this.resetForm, errors);
    const error = errors[0]!.name;

    this.error = this.messageService.getInputErrorMessage(error);
  }

}

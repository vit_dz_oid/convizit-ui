import { Component, OnInit, Inject } from '@angular/core';
import { PricingService } from './pricing.service';
import { Plan } from '../../interfaces/plan.interface';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'cv-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.scss']
})
export class PricingComponent implements OnInit {

  plans: Array<Plan> = [];
  isMonthly: boolean = false;

  constructor(
    private pricingService: PricingService,
    @Inject(DOCUMENT) private document
  ) { }

  ngOnInit() {
    this.pricingService.getPlans()
      .then(data => {
        this.plans = data;
      })
      .catch(errMsg => {
        console.log(errMsg);
      });
  }

  togglePeriod(value): void {
    if (value !== this.isMonthly) {
      this.isMonthly = value;
    }
  }

  activatePlan(planId): void {
    const isMonthly = this.isMonthly;
    this.pricingService.activatePlan({ planId, isMonthly })
      .then(data => {
        console.log(data);
        this.document.location.href = data;
      })
      .catch(errMsg => {
        console.log(errMsg);
      });
  }

}

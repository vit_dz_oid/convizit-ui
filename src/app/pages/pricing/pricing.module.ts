import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PricingRoutingModule } from './pricing-routing.module';
import { PricingComponent } from './pricing.component';
import { PricingService } from './pricing.service';
import { SharedModule } from '../../modules/shared.module';
import { WorkstageModule } from '../../modules/workstage/workstage.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    PricingRoutingModule,
  ],
  providers: [
    PricingService
  ],
  declarations: [PricingComponent]
})
export class PricingModule { }

import { Injectable } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Plan } from '../../interfaces/plan.interface';

@Injectable()
export class PricingService {

  constructor(
    private dataService: DataService
  ) { }

  getPlans(): Promise<Array<Plan>> {
    return new Promise<Array<Plan>>((resolve, reject) => {
      this.dataService.api({
        type: 'getPlans'
      }).subscribe(
        res => {
          if (!res.error) {
            resolve(res);
          } else {
            reject(res.error);
          }
        },
        err => {
          reject(err);
        }
      );
    });
  }

  activatePlan(planData: { planId: number, isMonthly: boolean }): Promise<string> {
    console.log(planData);
    return new Promise((resolve, reject) => {
      this.dataService.api({
        type: 'activatePlan',
        queryParams: Object.assign({}, planData)
      }).subscribe(
        res => {
          if (!res.error) {
            resolve(res);
          } else {
            reject(res.error);
          }
        },
        err => {
          console.log(err);
          reject(err);
        }
      );
    });
  }
}

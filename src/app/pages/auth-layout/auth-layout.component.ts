import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router, ChildActivationEnd, NavigationEnd } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { LoginStatus } from '../../interfaces/server.interface';

@Component({
  selector: 'cv-auth-layout',
  templateUrl: './auth-layout.component.html',
  styleUrls: ['./auth-layout.component.scss']
})
export class AuthLayoutComponent implements OnInit {

  title: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.title = this.activatedRoute.snapshot.firstChild.data.title;
      }
    });
  }

  ngOnInit() {
    
  }



}

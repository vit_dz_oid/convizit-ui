import { InactiveModule } from './inactive.module';

describe('InactiveModule', () => {
  let inactiveModule: InactiveModule;

  beforeEach(() => {
    inactiveModule = new InactiveModule();
  });

  it('should create an instance', () => {
    expect(inactiveModule).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InactiveRoutingModule } from './inactive-routing.module';
import { InactiveComponent } from './inactive.component';

@NgModule({
  imports: [
    CommonModule,
    InactiveRoutingModule
  ],
  declarations: [InactiveComponent]
})
export class InactiveModule { }

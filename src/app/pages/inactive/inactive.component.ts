import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cv-inactive',
  templateUrl: './inactive.component.html',
  styleUrls: ['./inactive.component.scss']
})
export class InactiveComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DataService } from '../../services/data.service';
import * as moment from 'moment-timezone';
import { SiteInfo } from '../../interfaces/server.interface';

@Component({
  selector: 'cv-project-settings',
  templateUrl: './project-settings.component.html',
  styleUrls: ['./project-settings.component.scss']
})
export class ProjectSettingsComponent implements OnInit {
  @Input() projectId = null;
  timezones = moment.tz.names();
  viewRanges = ['Yearly', 'Monthly', 'Weekly', 'Daily'];

  projectSettingsForm = this.fb.group({
    name: '',
    url: '',
    timezone: this.timezones[0],
    viewRange: 'Yearly',
    token: '',
  });


  constructor(
    private fb: FormBuilder,
    private dataService: DataService,
  ) { }

  ngOnInit() {   
    this.dataService.api({
      type: 'getSite',
      urlParams: {
        projectId: this.projectId
      }
    }).subscribe((res: SiteInfo) => {

      const { name, url, timezone, viewRange, token} = res;
      
      this.projectSettingsForm.setValue({ name, url, timezone, viewRange, token});
    });
  }
}

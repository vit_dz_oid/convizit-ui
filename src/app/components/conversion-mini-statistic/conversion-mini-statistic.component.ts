import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'cv-conversion-mini-statistic',
  templateUrl: './conversion-mini-statistic.component.html',
  styleUrls: ['./conversion-mini-statistic.component.scss']
})
export class ConversionMiniStatisticComponent {
  @Input() type = 'top-conversion';
  @Input() title = '';
  @Input() result: number = 0;
  @Input() subEvents:number = 1;
  @Input('subTitle') localSubTitle = null;
  @Input() eventId = '';

  highlighted = false;

  get subTitle() {
    return !!this.subEvents ? `${this.subEvents} Sub-events` : '';
  }

  setHighlight(val) {
    this.highlighted = val;
  }
}

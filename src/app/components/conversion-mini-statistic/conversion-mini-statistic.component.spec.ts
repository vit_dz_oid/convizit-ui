import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConversionMiniStatisticComponent } from './conversion-mini-statistic.component';

describe('ConversionMiniStatisticComponent', () => {
  let component: ConversionMiniStatisticComponent;
  let fixture: ComponentFixture<ConversionMiniStatisticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConversionMiniStatisticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConversionMiniStatisticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

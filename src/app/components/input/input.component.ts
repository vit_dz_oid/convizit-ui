import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, ValidatorFn } from '@angular/forms';

@Component({
  selector: 'cv-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: InputComponent, multi: true }
  ],
})
export class InputComponent implements OnInit, ControlValueAccessor {
  @Input() theme = 'common';
  @Input() disabled: boolean = false;
  @Input() placeholder: string = '';
  @Input() debounce = null;
  @Input() validators: ValidatorFn | ValidatorFn[];
  @Input() inputValue?: string;
  @Input() inputType?: string;
  @Input() type = 'text';
  @Input() showErrors = false;
  @Input() readonly = false;

  @Output() focus = new EventEmitter();
  @Output() blur = new EventEmitter();
  @Output() change = new EventEmitter();

  onInputValueChange: Function = () => { }
  timer = null;

  constructor() { }

  ngOnInit() {
  }

  handleChange(newVal) {
    if (this.debounce) {
      clearTimeout(this.timer);
      this.timer = setTimeout(() => {
        this.onInputValueChange(newVal);
        this.change.emit(newVal);
      }, +this.debounce);
    } else {
      this.onInputValueChange(newVal);
      this.change.emit(newVal);
    }
  }

  onFocus($event) {
    this.focus.emit($event);
  }

  onBlur($event) {
    this.blur.emit($event);
  }
  // Changed value from outside
  writeValue(val: string): void {
    this.inputValue = val;
  }
  registerOnChange(fn: any): void {
    this.onInputValueChange = fn;
  }
  registerOnTouched(fn: any): void {

  }
  setDisabledState?(isDisabled: boolean): void {

  }

}

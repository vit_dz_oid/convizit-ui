import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Component, Input, OnInit, forwardRef, ViewChild, ElementRef, HostListener } from '@angular/core';
import { BsDropdownDirective } from 'ngx-bootstrap/dropdown';

export const COMPONENT_NAME_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => SearchListComponent),
  multi: true
};

@Component({
  selector: 'cv-search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.scss'],
  providers: [COMPONENT_NAME_VALUE_ACCESSOR]
})
export class SearchListComponent implements ControlValueAccessor {
  @Input() list = [];
  @Input() theme = '';
  @ViewChild('dropdown', { read: BsDropdownDirective }) dropdownRef = null;

  @HostListener('body:click', ['$event'])
  // If click was out of the element
  closeList(event) {   
    if (this.isOpen) {
      if (!this.el.nativeElement.contains(event.target)) {
        this.hideList();
      }
    }
  }

  inputValue = '';// internal input value
  isOpen = false;

  private _value: any;
  //Selected value
  set value(value: any) {
    this._value = value;
    this.notifyValueChange();
  }

  get value() {
    return this._value;
  }

  get filteredList() {
    return this.list.filter((item: string) => item.toLocaleLowerCase().includes(this.inputValue.toLocaleLowerCase()));
  }

  onChange: (value) => {};
  onTouched: () => {};

  constructor(
    private el: ElementRef
  ) { }

  notifyValueChange() {
    if (this.onChange) {
      this.onChange(this.value);
    }
  }

  writeValue(obj: any): void {
    this.inputValue = obj;
    this.value = obj;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
  }

  updInputValue(e) {
    this.inputValue = e;
  }

  selectItem(item) {
    this.value = item;
    this.inputValue = item;

    this.hideList();
  }
  // List manipulation
  showList() {
    this.dropdownRef.show();
  }

  hideList() {
    this.dropdownRef.hide();
  }

  toggleShowList() {
    if(this.isOpen) {
      this.hideList();
    } else {
      this.showList();
    }
  }
  // Internal element event handlers
  onOpenChange(data: boolean): void {   
    this.isOpen = data;
  }

  onFocus() {
    !this.isOpen && this.showList();
  }

  onBlur() {
    // TODO: improve this shit
    setTimeout(
      () => {
        if (this.value !== this.inputValue) {
          this.inputValue = this.value;
        }
      }, 200);
  }
}

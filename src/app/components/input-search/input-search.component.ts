import { Component, Input, forwardRef, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

@Component({
  selector: 'cv-input-search',
  templateUrl: './input-search.component.html',
  styleUrls: ['./input-search.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => InputSearchComponent), multi: true
  }]
})
export class InputSearchComponent implements ControlValueAccessor {

  inputValue: string;

  @Input() type = 'text';
  @Input() theme = '';
  @Input() placeholder = '';
  @Input() disabled = false;
  @Input() enableIcon = false;
  @Input() inputType: string;
  @Input() maxLength: number;
  @Input() autocomplete: string;
  @Input() debounce: number = null;
  @Input() searchType = 'Search';
  @Input() closeType = 'Cancel';

  @Output() focus = new EventEmitter();
  @Output() blur = new EventEmitter();
  @Output() update = new EventEmitter();


  propagateChange: Function = () => { };

  constructor() { }

  handleChange(newVal) {
    this.propagateChange(newVal);
    this.update.emit(newVal);
  }

  writeValue(obj: any): void {
    this.inputType = obj;
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {

  }
  setDisabledState?(isDisabled: boolean): void {

  }

  onFocus($event) {
    this.focus.emit();
  }

  onBlur($event) {
    this.blur.emit();
  }

  clear() {
    this.inputValue = '';
    this.handleChange(this.inputValue);
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DataService } from '../../services/data.service';
import { Member } from '../../interfaces/server.interface';

@Component({
  selector: 'cv-team-members',
  templateUrl: './team-members.component.html',
  styleUrls: ['./team-members.component.scss']
})
export class TeamMembersComponent implements OnInit {
  search = '';
  showError = false;
  addMemberForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]]
  });
  get members(): Array<Member> {
    return this._members.filter(i => i.email.includes(this.search));
  }
  private _members: Array<Member> = [];

  constructor(
    private dataService: DataService,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.updMembers();
  }

  handleSearch(val) {
    this.search = val;
  }

  handleAddNewMember() {    
    if (this.addMemberForm.invalid) {
      this.showError = true;
      return;
    }

    this.showError = false;

    this.dataService.api({
      type: 'inviteMember',
      data: this.addMemberForm.value
    }).subscribe(() => {
      this.addMemberForm.get('email').setValue('');
      this.updMembers();
    });
  }

  updMembers() {
    this.dataService.api({
      type: 'allMemebers'
    }).subscribe((res: Array<Member>) => {     
      this._members = res;
    });
  }

  handleDeleteMember(email) {
    debugger;
    this.dataService.api({
      type: 'deleteMember',
      urlParams: {
        member: email,
      }
    }).subscribe(() => {
      this.updMembers();
    });
  }
}

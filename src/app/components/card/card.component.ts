import { Component, Input } from '@angular/core';

@Component({
  selector: 'cv-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {
  @Input() type = 'common';
  @Input() title = '';
  @Input() mainText = '';
  @Input() labelText = '';
  @Input() subTitle = '';
}

import { Component,
  Input,
  TemplateRef,
  HostListener,
} from '@angular/core';

@Component({
  selector: 'cv-tooltip-container',
  templateUrl: './tooltip-container.component.html',
  styleUrls: ['./tooltip-container.component.scss'],
})
export class CVTooltipContainerComponent {
  @Input() popTemplate: TemplateRef<any> = null;
  @Input() containerClass: string = null;
  @Input() triggers = "";
  @Input() delay = 750;
  @Input() placement = "bottom";
  @Input() theme = "";

  isOpen = false;
  private _timeOut = null;

  @HostListener('mouseenter', ['$event'])
  handleMouseEnter(event: Event) {
    
    if (!this.isOpen) {     
      this._timeOut = setTimeout(() => this.isOpen = true, this.delay);
    }
  }

  @HostListener('mouseleave', ['$event'])
  handleMouseLeave(event: Event) {
    clearTimeout(this._timeOut);

    if (this.isOpen) {
      this.isOpen = false;
    }
  }
}

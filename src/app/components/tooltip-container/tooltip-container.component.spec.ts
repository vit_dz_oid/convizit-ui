import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CVTooltipContainerComponent } from './tooltip-container.component';

describe('TooltipContainerComponent', () => {
  let component: CVTooltipContainerComponent;
  let fixture: ComponentFixture<CVTooltipContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CVTooltipContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CVTooltipContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

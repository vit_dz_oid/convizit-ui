import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'cv-percents',
  templateUrl: './percents.component.html',
  styleUrls: ['./percents.component.scss']
})
export class PercentsComponent {
  @Input() value = 0;
  @Input() type = '';
  @Input() default = 'New';
}

import { Component, forwardRef, Input, SimpleChange } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, FormControl } from '@angular/forms';
import { startWith } from 'rxjs/operators';

@Component({
  selector: 'cv-input-error',
  templateUrl: './input-error.component.html',
  styleUrls: ['./input-error.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => InputErrorComponent), multi: true }
  ]
})
export class InputErrorComponent implements ControlValueAccessor {
  @Input() type = 'text';
  @Input() placeholder = '';
  @Input() formCtrl: FormControl;
  @Input() disabled = false;
  @Input() enableIcon = false;
  @Input() showErrors = false;
  @Input() showErrorsOnBlur = false;
  @Input() showErrorOnEnter = false;
  @Input() inputType: string;
  @Input() maxLength: number;
  @Input() autocomplete: string;
  @Input() debounce: number = null;

  subs = [];
  propagateChange: Function = () => { };
  inputValue: string;
  errorClasses: any;
  private _subs = [];

  constructor() { }

  ngOnChanges(changes: SimpleChange) {

  }

  /* ngAfterContentInit() {
    this._updErrorClasses();

    this._subs.push(this.formCtrl.statusChanges.pipe(startWith('')).subscribe(newStatus => {
      this._updErrorClasses();
    }));
  } */

  writeValue(obj: any): void {
    this.inputType = obj;
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {

  }
  setDisabledState?(isDisabled: boolean): void {

  }

  onFocus($event) {
    if (this.showErrorsOnBlur || this.showErrorOnEnter) {
      this.showErrors = false;
    }
  }

  onBlur($event) {
    if (this.showErrorsOnBlur) {
      this.showErrors = true;
    }
  }

  private _updErrorClasses() {
    this.errorClasses = {
      'ng-invalid': this.formCtrl.invalid,
      'ng-valid': this.formCtrl.valid,
      'ng-dirty': this.formCtrl.dirty,
      'ng-pristine': this.formCtrl.pristine
    };
  }

}

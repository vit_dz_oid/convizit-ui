import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceFaviconComponent } from './source-favicon.component';

describe('SourceFaviconComponent', () => {
  let component: SourceFaviconComponent;
  let fixture: ComponentFixture<SourceFaviconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourceFaviconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourceFaviconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'cv-source-favicon',
  templateUrl: './source-favicon.component.html',
  styleUrls: ['./source-favicon.component.scss']
})
export class SourceFaviconComponent implements OnInit {

  @Input() sourceUrl: string;

  constructor() { }

  ngOnInit() {
    this.sourceUrl = `https://convizit-favicon.herokuapp.com/icon?url=${this.sourceUrl}&size=16..32..64`;
  }

}

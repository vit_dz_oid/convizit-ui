import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'cv-referrer-statistic',
  templateUrl: './referrer-statistic.component.html',
  styleUrls: ['./referrer-statistic.component.scss']
})
export class ReferrerStatisticComponent {
  @Input() type = '';
  @Input() title = '';
  @Input() result: number = 0;
  @Input() items:number = 1;
  @Input() link = '';
  @Input() imgType = null;
}

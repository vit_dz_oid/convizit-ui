import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferrerStatisticComponent } from './referrer-statistic.component';

describe('ReferrerStatisticComponent', () => {
  let component: ReferrerStatisticComponent;
  let fixture: ComponentFixture<ReferrerStatisticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferrerStatisticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferrerStatisticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

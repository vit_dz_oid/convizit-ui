import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstallCodeComponent } from './install-code.component';

describe('InstallCodeComponent', () => {
  let component: InstallCodeComponent;
  let fixture: ComponentFixture<InstallCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstallCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstallCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'cv-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss']
})
export class IconComponent implements OnInit {
  @Input() type = 'none';

  constructor() { }

  ngOnInit() {
  }

}

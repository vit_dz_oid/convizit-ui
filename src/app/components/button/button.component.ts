import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'cv-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() theme = 'common';
  @Input() buttonType = null;
  @Input() disabled = false;

  constructor() { }

  ngOnInit() {
  }

}

import { Component, Input, ViewEncapsulation } from '@angular/core';
import { PATHS } from '../../maps/config.map';

const SVG_SIZES = {
  sm:  { 'width': '12px', 'height': '12px'},
  md: { 'width': '16px', 'height': '16px'},
  lg:  { 'width': '18px', 'height': '18px'}
};
@Component({
  selector: 'cv-svg',
  templateUrl: './svg.component.html',
  styleUrls: ['./svg.component.scss']
})
export class SvgComponent {
  @Input() set type(val) {
    this._src = val;
  }
  @Input() stretch: boolean = false;
  @Input() svgStyle: {[key: string]: string|number} = null;
  @Input() theme: string = '';
  @Input() size: "sm" | "md" | "lg" = "md";

  get style() {
    return  Object.assign({}, SVG_SIZES[this.size], this.svgStyle);
  }

  private _src = '';

  get type() {
    return `${PATHS.svgsPath}${this._src}.svg`;
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescriberWithActionComponent } from './describer-with-action.component';

describe('DescriberWithActionComponent', () => {
  let component: DescriberWithActionComponent;
  let fixture: ComponentFixture<DescriberWithActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescriberWithActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescriberWithActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

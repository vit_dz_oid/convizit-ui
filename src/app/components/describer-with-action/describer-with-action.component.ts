import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'cv-describer-with-action',
  templateUrl: './describer-with-action.component.html',
  styleUrls: ['./describer-with-action.component.scss']
})
export class DescriberWithActionComponent implements OnInit {
  @Input() title = '';
  @Input() subTitle = '';
  @Input() actionType = null;
  @Input() actionStyle = null;
  @Input() theme: 'common' = 'common';

  @Output() onTitleClick = new EventEmitter();
  @Output() onSubTitleClick = new EventEmitter();
  @Output() onActionClick = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  handleTitleClick() {
    this.onTitleClick.emit();
  }

  handleSubTitleClick() {
    this.onSubTitleClick.emit();
  }

  handleActionClick() {
    this.onActionClick.emit();
  }
}

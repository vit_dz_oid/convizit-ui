import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConversionTooltipInfoComponent } from './conversion-tooltip-info.component';

describe('ConversionTooltipInfoComponent', () => {
  let component: ConversionTooltipInfoComponent;
  let fixture: ComponentFixture<ConversionTooltipInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConversionTooltipInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConversionTooltipInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

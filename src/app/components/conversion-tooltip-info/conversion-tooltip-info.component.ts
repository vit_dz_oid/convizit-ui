import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Event } from '../../interfaces/server.interface';

//TODO: Dima - improve css

@Component({
  selector: 'cv-conversion-tooltip-info',
  templateUrl: './conversion-tooltip-info.component.html',
  styleUrls: ['./conversion-tooltip-info.component.scss'],
})
export class ConversionTooltipInfoComponent implements OnInit {
  @Input() eventId = '';
  @Input() range = '';
  
  title = null;
  subTitle = null;
  conversionEnabled = false;
  totalClicks = null;
  convRate = null;

  loading = true;

  constructor(
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.loading = true;

    this.dataService.api({
      type: 'event',
      urlParams: {
        event: this.eventId
      },
      queryParams: {
        range: 'Yearly'
      }
    }).subscribe((event: Event) => {
      this.title = event.name;
      this.subTitle = event.pageTitle;
      this.conversionEnabled = event.isConversion;
      this.totalClicks = event.totalClicks;
      this.convRate = event.conversionRate;

      this.loading = false;
    }, err => console.warn('Catch error:', err));
  }

  toggleConversion(val) {
    if (val === this.conversionEnabled) return;   

    this.dataService.api({
      type: val ? 'setConversion' : 'unsetConversion',
      data: {
        eventIds: this.eventId
      }
    }).subscribe((event: Event) => {
      this.conversionEnabled = val;
    }, err => console.warn('Catch error:', err));
  }
}

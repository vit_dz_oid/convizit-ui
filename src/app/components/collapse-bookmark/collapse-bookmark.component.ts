import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'cv-collapse-bookmark',
  templateUrl: './collapse-bookmark.component.html',
  styleUrls: ['./collapse-bookmark.component.scss']
})
export class CollapseBookmarkComponent {
  @Input() type: 'common' = 'common';

  isOpen = false;

  toggleComponent() {
    this.isOpen = !this.isOpen;
  }
}

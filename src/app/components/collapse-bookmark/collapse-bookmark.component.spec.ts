import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollapseBookmarkComponent } from './collapse-bookmark.component';

describe('CollapseBookmarkComponent', () => {
  let component: CollapseBookmarkComponent;
  let fixture: ComponentFixture<CollapseBookmarkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollapseBookmarkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollapseBookmarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

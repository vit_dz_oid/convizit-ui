import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'cv-action-label',
  templateUrl: './action-label.component.html',
  styleUrls: ['./action-label.component.scss']
})
export class ActionLabelComponent {
  @Input() type = null;
  @Input() title = "";
  @Input() theme = "";
  @Input() svgStyle = {};

}

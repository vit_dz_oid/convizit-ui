import { Component, Input } from '@angular/core';

@Component({
  selector: 'cv-mini-statistic',
  templateUrl: './ministatistic.component.html',
  styleUrls: ['./ministatistic.component.scss']
})
export class MinistatisticComponent {
  @Input() type = '';
  @Input() title = '';
  @Input() result = '';
  @Input() items:number = 1;
  @Input() subTitle = '';
  @Input() svgType = '';
  @Input() imgType = null;
  @Input() selector = null;
  @Input() loading = false;

  get locItems(): number[] {
    if(this.items != this._items.length) {
      this._items = new Array<number>(+this.items).fill(1);
    }
    
    return this._items.slice(0,4);
  }
  
  private _items: number[] = [];
}

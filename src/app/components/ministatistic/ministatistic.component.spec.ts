import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinistatisticComponent } from './ministatistic.component';

describe('MinistatisticComponent', () => {
  let component: MinistatisticComponent;
  let fixture: ComponentFixture<MinistatisticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinistatisticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinistatisticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

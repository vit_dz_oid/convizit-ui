import { Component, OnInit, Input } from '@angular/core';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

@Component({
  selector: 'cv-tracking-code',
  templateUrl: './tracking-code.component.html',
  styleUrls: ['./tracking-code.component.scss']
})
export class TrackingCodeComponent implements OnInit {

  @Input() trackingToken: string;
  public config: PerfectScrollbarConfigInterface = {
    wheelSpeed: 0.5,
    minScrollbarLength: 10
  };

  constructor() { }

  ngOnInit() {
  }

}

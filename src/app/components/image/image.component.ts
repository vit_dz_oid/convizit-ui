import { Component, Input } from '@angular/core';

@Component({
  selector: 'cv-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent {
  @Input() theme = '';
  @Input() set type(val) {
    if (val && val.match(/\:\/\//gm)) { //if url - takes from url
      this._locType = `https://convizit-favicon.herokuapp.com/icon?url=${val}&size=16..32..64`;
    } else { // takes from local storage
      this._locType = val; 
    }
  }

  protected _locType = null;
}

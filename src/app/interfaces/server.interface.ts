/****************************************************************
 * Contains the interfaces and mocks of all server responses
 ****************************************************************/

export interface Events { }

export interface Error { }

export interface LoginStatus {
    isLogin: boolean,
    isVerified: boolean,
    isActive: boolean,
    trialDays: boolean,
    isOwner: boolean,
}

export interface StatisticAnomaly {
    change: number,
    conversionRate: number,
    topElementChanges: any[],
    topReferrerChanges: any[],
    total: number,
    unique: number,
}

export interface AnomalyEl {
    id: number,
    imageUrl: string,
    isConversion: number,
    name: string,
    subElementCount: number,
}

export interface Anomaly {
    bucketSpan: number,
    element: AnomalyEl,
    id: number,
    isPinned: number,
    timesHigher: number,
    timestamp: number,
    type: string
}

export interface Site {
    creationDate: string,
    id: number,
    isOwner: number,
    name: string,
    timezone: string,
    url: string
    viewRange: string
}
export interface AvailableSite extends Site {
    isSelected: number,
}

export interface SiteInfo extends Site {
    token: string,
}

export interface Event {
    id: number,
    conversionRate: number,
    groupId: string,
    imageUrl: string,
    isConversion: boolean,
    name: string,
    pageTitle: string,
    subEventsCount: number,
    subevents: any,
    totalClicks: number,
    uniqueClicks: number,
    uniqueVisitors: number
}

export interface TrafficSource {
    id: number
    bounceRate: number,
    bounceVisitors: number,
    conversionRate: number,
    conversions: number,
    name: string,
    url: string,
    visitors: number,
    urls: Array<TrafficSource>
}


export interface Member {
    email: string,
    isOwner: number,
    isPending: number,
    name: string,
    userId: number
}

export interface Plan {
    id: number,
    name: string,
    description: string,
    price: number,
    isSelected: number,
    isAvailable: number,
    isRecommended: number,
    maxMemberCount: number,
    maxProjectCount: number,
    maxSessionCount: number
}
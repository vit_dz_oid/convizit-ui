import * as server from './server.interface';

export interface ApiMap {
    [type: string]: ApiMapItem;
}

export interface ApiMapItem {
    url: string;
    method: "get" | "post" | "put" | "delete";
    mockup?: any;
    errMockup?: server.Error;
    guestMode?: any;
    errors?: {
        [name: number]: string;
    };
    headers?: {
        [name: string]: string;
    };
    responseType?: string;
}
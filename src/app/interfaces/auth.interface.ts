export interface LoginResponse {
    isLogin: boolean,
    isVerified: boolean
}

export interface CheckTokenResponse {
    userId: number,
    userEmail: string,
    iat: number,
    exp: number
}

export interface EmailUsedResponse {
    status: boolean
}
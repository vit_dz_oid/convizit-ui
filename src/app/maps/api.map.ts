import * as maps from '../interfaces/maps.interface';
import * as mocks from '../interfaces/server.mocks';

export const apiMap: maps.ApiMap = {
    getEvents: {
        url: '/api/events/<%= urlParams.event %>',
        method: 'get',
        mockup: mocks.SERVER_EVENTS_MOCK
    },
    login: {
        url: '/api/login',
        method: 'post',
        /* mockup: mocks.SERVER_LOGIN, */
    },
    signup: {
        url: '/api/register',
        method: 'post',
    },
    resetPassword: {
        url: '/api/forget',
        method: 'post',
    },
    checkResetToken: {
        url: '/api/forget/token',
        method: 'post',
    },
    setPassword: {
        url: '/api/set-password',
        method: 'post',
    },
    checkEmailUsed: {
        url: '/api/check-email',
        method: 'post'
    },
    isLogged: {
        url: '/api/is-login',
        method: 'get'
    },
    isVerified: {
        url: 'api/common/is-verified',
        method: 'post'
    },
    allSites: {
        url: '/api/site/all',
        method: 'get'
    },
    /**
     * &range
     */
    statisticAnomaly: {
        url: '/api/statistic/anomaly',
        method: 'get'
    },
    /**
     * ?conversion: true/false
     * &pinned: true/false
     * &range: Yearly/Monthly/Weekly/Daily
     */
    anomaly: {
        url: '/api/anomaly',
        method: 'get'
    },
    selectSite: {
        url: '/api/site/<%= urlParams.site%>/select',
        method: 'post'
    },

    getSite: {
        url: '/api/site/<%= urlParams.projectId %>',
        method: 'get'
    },
    /**
     * ?range: Yearly/Monthly/Weekly/Daily
     */
    event: {
        url: '/api/event/<%= urlParams.event%>',
        method: 'get'
    },

    unsetConversion: {
        url: '/api/event/unset-as-conversion',
        method: 'put'
    },

    setConversion: {
        url: '/api/event/set-as-conversion',
        method: 'put'
    },

    getPlans: {
        url: '/api/subscription/plans',
        method: 'get'
    },

    activatePlan: {
        url: '/api/subscription/activate',
        method: 'get'
    },

    getTrafficSources: {
        url: 'api/referrer',
        method: 'get'
    },

    allMemebers: {
        url: '/api/member/all',
        method: 'get'
    },
    /** Body params:
     * @email
     */
    inviteMember: {
        url: '/api/member/invite',
        method: 'post'
    },
    deleteMember: {
        url: '/api/member/<%= urlParams.member%>',
        method: 'delete'
    }
};

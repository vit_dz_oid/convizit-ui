export const modalMap = {
    common: {
        selector: '<component selector here "cv-common-modal">',
        title: 'Common', // can be some with i18n service like a 'concrete.component.field'
        options: {
            backdrop: false,
            centered: true,
            width: 394,
            marginTop: 123,
            theme: ''
        },
        btns: [
            { id: 'close', label: 'Close', type: 'type-2', wrapperClass: ['footer-btns'], cssClass: ['text-center'] },
            { id: 'ok', label: 'Ok', type: 'type-2', wrapperClass: ['footer-btns'], cssClass: ['text-center'] }
        ]
    },
    notification: {
        selector: 'cv-base-alert-modal',
        title: 'Notification',
        options: {
            backdrop: false,
            centered: true,
            theme: 'notification'
        },
        btns: [
            { id: 'ok', label: 'Close', type: 'type-2', wrapperClass: ['footer-btns'], cssClass: ['text-center'] }
        ]
    },
    error: {
        selector: 'cv-base-alert-modal',
        title: 'Error',
        options: {
            backdrop: false,
            centered: true,
            theme: 'error'
        },
        btns: [
            { id: 'ok', label: 'Ok', type: 'type-2', wrapperClass: ['footer-btns'], cssClass: ['text-center'] }
        ]
    },
    warning: {
        selector: 'cv-base-alert-modal',
        title: 'Warning',
        options: {
            backdrop: false,
            centered: true,
            theme: 'warning'
        },
        btns: [
            { id: 'close', label: 'Close', type: 'type-2', wrapperClass: ['footer-btns'], cssClass: ['text-center'] },
            { id: 'ok', label: 'Continue', type: 'type-1', wrapperClass: ['footer-btns'], cssClass: ['text-center'] }
        ]
    },
    confirmation: {
        selector: 'cv-base-alert-modal',
        title: 'Confirmation',
        options: {
            backdrop: false,
            centered: true,
            theme: 'Confirmation'
        },
        btns: [
            { id: 'close', label: 'Cancel', type: 'type-2', wrapperClass: ['footer-btns'], cssClass: ['text-center'] },
            { id: 'ok', label: 'Continue', type: 'type-1', wrapperClass: ['footer-btns'], cssClass: ['text-center'] }
        ]
    },
    projectConfig: {
        selector: 'cv-project-config-modal',
        options: {
            width: 574,
            marginTop: 125,
            theme: 'project-settings',
        },
        dialogOptions: {
            closeByClickingOutside: true,
        }
    },
    installCode: {
        selector: 'cv-install-code-modal',
        options: {
            width: 714,
            marginTop: 110,
            theme: 'install-code'
        },
        title: 'Installing Convizit Plugin On Your Website'
    }
};

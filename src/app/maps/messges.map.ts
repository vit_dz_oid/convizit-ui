export const messages = {
    errors: {
        input: {
            required: 'Field is required',
            requiredUsername: 'Username is required',
            requiredName: 'Name is required',
            requiredEmail: 'Email is required',
            usernameEmailPattern: 'It`s not valid email address',
            emailPattern: 'It`s not valid email address',
            requiredPassword: 'Password is required',
        }
    }
}
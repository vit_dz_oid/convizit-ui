import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRouting } from './app.routing';
import { BootstrapModalModule } from './vendor/ng2-bootstrap-modal/bootstrap-modal.module';
import { ModalWrapperComponent } from './modals/modal-wrapper.component';
import { InterceptorsModule } from './modules/interceptors.module';
import { CoreModule } from './modules/core.module';
import { SharedModule } from './modules/shared.module';
@NgModule({
  declarations: [
    AppComponent,
    ModalWrapperComponent,
  ],
  entryComponents: [
    ModalWrapperComponent
  ],
  imports: [
    SharedModule,  // contains all shared components/pipes/directives
    CoreModule,    // contains all services
    BrowserModule,
    HttpClientModule,
    BootstrapModalModule,
    AppRouting,  // This should be the last entry in the imports array.
  ],
  providers: [ InterceptorsModule ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { TestBed, inject } from '@angular/core/testing';

import { WorkstageService } from './workstage.service';

describe('WorkstageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorkstageService]
    });
  });

  it('should be created', inject([WorkstageService], (service: WorkstageService) => {
    expect(service).toBeTruthy();
  }));
});

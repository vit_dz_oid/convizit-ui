import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkstagePage } from './workstage.page';
import { IsVerifiedGuard } from '../../guards/is-verified.guard';
import { IsNotVerifiedGuard } from '../../guards/is-not-verified.guard';
import { IsActiveGuard } from '../../guards/is-active.guard';
import { IsMemberGuard } from '../../guards/is-member.guard';
import { IsOwnerGuard } from '../../guards/is-owner.guard';
import { IsNotActivatedGuard } from '../../guards/is-not-activated.guard';

const routes: Routes = [
  {
    path: '',
    component: WorkstagePage,
    children: [
      {
        path: 'news',
        loadChildren: '../../pages/news/news.module#NewsModule',
        canActivate: [IsActiveGuard, IsVerifiedGuard],
        runGuardsAndResolvers: 'always',
      },
      {
        path: 'events',
        loadChildren: '../../pages/events/events.module#EventsModule',
        canActivate: [IsActiveGuard, IsVerifiedGuard]
      },
      {
        path: 'traffic',
        loadChildren: '../../pages/traffic/traffic.module#TrafficModule',
        canActivate: [IsActiveGuard, IsVerifiedGuard]
      },
      {
        path: 'event-details',
        loadChildren: '../../pages/event-details/event-details.module#EventDetailsModule',
        canActivate: [IsActiveGuard, IsVerifiedGuard]
      },
      {
        path: 'pricing',
        loadChildren: '../../pages/pricing/pricing.module#PricingModule',
        canActivate: [IsOwnerGuard]
      },
      {
        path: 'boarding',
        loadChildren: '../../pages/boarding/boarding.module#BoardingModule',
        canActivate: [IsActiveGuard, IsNotVerifiedGuard]
      },
      {
        path: 'inactive',
        loadChildren: '../../pages/inactive/inactive.module#InactiveModule',
        canActivate: [IsNotActivatedGuard, IsMemberGuard]
      },
      {
        path: '**',
        redirectTo: 'news'
      }
    ]
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkstageRoutingModule { }

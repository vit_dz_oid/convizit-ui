import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { DataService } from '../../services/data.service';
import { AvailableSite } from '../../interfaces/server.interface';
import { NotificationService } from '../../services/notification.service';

@Injectable({
  providedIn: 'root'
})
export class WorkstageService {
  /**
   * Selected sites
   */
  protected _selectedSites$ = new BehaviorSubject<any>(null);
  selectedSites$ = this._selectedSites$.asObservable();
  get sites(): Array<AvailableSite> { return this._selectedSites$.getValue(); }
  /**
   * Period of selection
   */
  protected _selectedPeriod$ = new BehaviorSubject<any>(null);
  selectedPeriod$ = this._selectedPeriod$.asObservable();
  get period() { return this._selectedPeriod$.getValue(); }

  constructor(
    private dataService: DataService,
    private notificationService: NotificationService
  ) { }

  getAvailableSites() {
    return this.dataService.api({
      type: 'allSites'
    }).pipe(tap((res: Array<AvailableSite>) => {
      this._selectedSites$.next(res);

      const site = res.find(item => !!item.isSelected);
      this._selectedPeriod$.next(site.viewRange);
    }));
  }

  selectSite(id) {
    return this.dataService.api({
      type: 'selectSite',
      urlParams: {
        site: id
      }
    }).pipe(tap(() => {
      this.notificationService.sendSiteChangeMessage(true);
    }));
  }

  selectPeriod(period) {
    this._selectedPeriod$.next(period);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkstagePage } from './workstage.page';

describe('WorkstagePage', () => {
  let component: WorkstagePage;
  let fixture: ComponentFixture<WorkstagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkstagePage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkstagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

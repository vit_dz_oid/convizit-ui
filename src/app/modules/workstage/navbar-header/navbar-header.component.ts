import { Component, OnInit } from '@angular/core';
import { WorkstageService } from '../workstage.service';
import { AvailableSite } from '../../../interfaces/server.interface';
import { NotificationService } from '../../../services/notification.service';
import { ModalService } from '../../../services/modal.service';

@Component({
  selector: 'cv-navbar-header',
  templateUrl: './navbar-header.component.html',
  styleUrls: ['./navbar-header.component.scss']
})
export class NavbarHeaderComponent implements OnInit {
  sites: Array<AvailableSite> = null;
  selectedSite: AvailableSite = null;
  periods = ['Yearly', 'Monthly', 'Weekly', 'Daily'];

  constructor(
    protected workstageService: WorkstageService,
    private modalService: ModalService
  ) {  }

  ngOnInit(): void {
    this._updAvaliableSites();
  }

  selectSite(id: number) {
    this.workstageService.selectSite(id).subscribe(res => {
      this._updAvaliableSites();
    });
  }

  showConfigModal(id: number) {
    this.modalService.set({
      type: 'projectConfig',
      args: {
        projectId: id
      }
    }).subscribe();
  }

  selectPeriod(period: string) {   
    this.workstageService.selectPeriod(period);
  }

  private _updAvaliableSites() {
    this.workstageService.getAvailableSites().subscribe((res: Array<AvailableSite>) => {
      this.sites = res;
      this.selectedSite = this.sites.find(site => !!site.isSelected);
    });
  }
}

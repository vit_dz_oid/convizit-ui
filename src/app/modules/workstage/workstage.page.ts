import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { LoginStatus } from '../../interfaces/server.interface';
import { Router } from '@angular/router';
import { WorkstageService } from './workstage.service';

@Component({
  selector: 'cv-workstage',
  templateUrl: './workstage.page.html',
  styleUrls: ['./workstage.page.scss']
})
export class WorkstagePage implements OnInit {
  loginChecked = false;
  get sitesInfoReady() {
    return !!this.workstageService.sites;
  }

  constructor(
    private workstageService: WorkstageService,
    private authService: AuthService,
    private routing: Router
  ) { }

  ngOnInit() {
  }
}

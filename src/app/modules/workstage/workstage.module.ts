import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkstageRoutingModule } from './workstage-routing.module';
import { SharedModule } from '../shared.module';
import { WorkstagePage } from './workstage.page';
import { NavbarHeaderComponent } from './navbar-header/navbar-header.component';
import { WorkstageService } from './workstage.service';
import { EventsModule } from '../../pages/events/events.module';
import { TrafficModule } from '../../pages/traffic/traffic.module';
import { EventDetailsModule } from '../../pages/event-details/event-details.module';
import { PricingModule } from '../../pages/pricing/pricing.module';
import { BoardingModule } from '../../pages/boarding/boarding.module';
import { InactiveModule } from '../../pages/inactive/inactive.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    WorkstageRoutingModule,
    EventsModule,
    TrafficModule,
    EventDetailsModule,
    PricingModule,
    BoardingModule,
    InactiveModule,
  ],
  declarations: [
    WorkstagePage,
    NavbarHeaderComponent,
  ],
})
export class WorkstageModule { }

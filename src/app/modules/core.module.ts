import { NgModule, Optional, SkipSelf } from '@angular/core';

import { ModalService } from '../services/modal.service';
import { FormService } from '../services/form.service';
import { MessageService } from '../services/message.service';
import { DataService } from '../services/data.service';
import { AuthService } from '../services/auth.service';
import { PercentPipe } from '@angular/common';
import { NotificationService } from '../services/notification.service';
 
// Used for common services
@NgModule({
    providers: [
        ModalService,
        FormService,
        MessageService,
        DataService,
        NotificationService,
        AuthService,
        PercentPipe,
    ]
})
export class CoreModule {
    // This guards against the CoreModule from being imported into any
    // module except the root-module (AppModule).
    constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
        if (parentModule) {
            throw new Error('CoreModule is already loaded. Import it in the AppModule only');
        }
    }
}
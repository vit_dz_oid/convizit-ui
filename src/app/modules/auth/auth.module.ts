import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginPage } from '../../pages/login/login.page';
import { SharedModule } from '../shared.module';
import { AuthLayoutComponent } from '../../pages/auth-layout/auth-layout.component';
import { RegisterPage } from '../../pages/register/register.component';
import { ResetPasswordPage } from '../../pages/reset-password/reset-password.component';
import { SetPasswordPage } from '../../pages/set-password/set-password.component';
import { NgxCaptchaModule } from 'ngx-captcha';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AuthRoutingModule,
    NgSelectModule,
    NgxCaptchaModule.forRoot({
      reCaptcha2SiteKey: '6Lf_ZkEUAAAAAKPjzqx4QttxZ3CeGDrAPBHLBrAq'
    }),
  ],
  declarations: [
    LoginPage,
    AuthLayoutComponent,
    RegisterPage,
    ResetPasswordPage,
    SetPasswordPage
  ]
})
export class AuthModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPage } from '../../pages/login/login.page';
import { RegisterPage } from '../../pages/register/register.component';
import { AuthLayoutComponent } from '../../pages/auth-layout/auth-layout.component';
import { ResetPasswordPage } from '../../pages/reset-password/reset-password.component';
import { SetPasswordPage } from '../../pages/set-password/set-password.component';

const routes: Routes = [
  {
    path: '',
    component: AuthLayoutComponent,
    children: [
      {
        path: 'login',
        component: LoginPage,
        data: { title: 'Login' }
      },
      {
        path: 'register',
        component: RegisterPage,
        data: { title: 'Sign Up' }
      },
      {
        path: 'reset-password',
        component: ResetPasswordPage,
        data: { title: 'Password recovery' }
      },
      {
        path: 'set-password/:token',
        component: SetPasswordPage,
        data: { title: 'Password recovery' }
      },
      {
        path: '**',
        redirectTo: 'login'
      }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }

import { NgModule, Optional, SkipSelf } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { AuthHeaderInterceptorService } from '../interceptors/auth-header-interceptor.service';

@NgModule({
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthHeaderInterceptorService,
            multi: true,
        },
    ]
})
export class InterceptorsModule {
    constructor( @Optional() @SkipSelf() parentModule: InterceptorsModule) {
        if (parentModule) {
            throw new Error('InterceptorsModule is already loaded. Import it in the AppModule only');
        }
    }
}
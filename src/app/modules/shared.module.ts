import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { BootstrapModalModule } from '../vendor/ng2-bootstrap-modal/bootstrap-modal.module';
import { CVBaseAlertModalComponent } from '../modals/base-alert/base-alert-modal.component';
import { ButtonComponent } from '../components/button/button.component';
import { IconComponent } from '../components/icon/icon.component';
import { InputComponent } from '../components/input/input.component';
import { SvgComponent } from '../components/svg/svg.component';
import { InputErrorComponent } from '../components/input-error/input-error.component';
import { DescriberWithActionComponent } from '../components/describer-with-action/describer-with-action.component';
import { ActionLabelComponent } from '../components/action-label/action-label.component';
import { CollapseBookmarkComponent } from '../components/collapse-bookmark/collapse-bookmark.component';
import { CardComponent } from '../components/card/card.component';
import { ConversionMiniStatisticComponent } from '../components/conversion-mini-statistic/conversion-mini-statistic.component';
import { MinistatisticComponent } from '../components/ministatistic/ministatistic.component';
import { ReferrerStatisticComponent } from '../components/referrer-statistic/referrer-statistic.component';
import { ImageComponent } from '../components/image/image.component';
import { SignedPercentPipe } from '../pipes/signed-percent.pipe';
import { EllipsisDirective } from '../directives/ellipsisi.directive';
import { PercentsComponent } from '../components/percents/percents.component';
import { FromNowPipe } from '../pipes/from-now.pipe';
import { InputSearchComponent } from '../components/input-search/input-search.component';
import { ConversionTooltipInfoComponent } from '../components/conversion-tooltip-info/conversion-tooltip-info.component';
import { TogglerComponent } from '../components/toggler/toggler.component';
import { CVTooltipContainerComponent } from '../components/tooltip-container/tooltip-container.component';
import { PeriodPricePipe } from '../pipes/period-price.pipe';
import { ProjectSettingsComponent } from '../components/project-settings/project-settings.component';
import { SearchListComponent } from '../components/search-list/search-list.component';
import { ProjectConfigModalComponent } from '../modals/project-config-modal/project-config-modal.component';
import { SelectComponent } from '../components/select/select.component';
import { TrackingCodeComponent } from '../components/tracking-code/tracking-code.component';
import { InstallCodeModalComponent } from '../modals/install-code-modal/install-code-modal.component';
import { InstallCodeComponent } from '../components/install-code/install-code.component';
import { SourceFaviconComponent } from '../components/source-favicon/source-favicon.component';
import { RoundPipe } from '../pipes/round.pipe';
import { TeamMembersComponent } from '../components/team-members/team-members.component';

@NgModule({
  declarations: [
    CVBaseAlertModalComponent,
    ProjectConfigModalComponent,
    InstallCodeModalComponent,
    ButtonComponent,
    IconComponent,
    InputComponent,
    SvgComponent,
    InputErrorComponent,
    DescriberWithActionComponent,
    ActionLabelComponent,
    CollapseBookmarkComponent,
    CardComponent,
    ConversionMiniStatisticComponent,
    MinistatisticComponent,
    ReferrerStatisticComponent,
    ImageComponent,
    PercentsComponent,
    InputSearchComponent,
    ConversionTooltipInfoComponent,
    TogglerComponent,
    CVTooltipContainerComponent,
    ProjectSettingsComponent,
    InstallCodeComponent,
    SearchListComponent,
    SelectComponent,
    TrackingCodeComponent,
    TeamMembersComponent,
    SourceFaviconComponent,
    // pipes
    SignedPercentPipe,
    FromNowPipe,
    PeriodPricePipe,
    RoundPipe,
    // directives
    EllipsisDirective,
  ],
  entryComponents: [
    CVBaseAlertModalComponent,
    ProjectConfigModalComponent,
    InstallCodeModalComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    BootstrapModalModule,
    AngularSvgIconModule,
    PerfectScrollbarModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    BootstrapModalModule,
    AngularSvgIconModule,
    BsDropdownModule,
    CVBaseAlertModalComponent,
    ProjectConfigModalComponent,
    InstallCodeModalComponent,
    ButtonComponent,
    IconComponent,
    InputComponent,
    SvgComponent,
    InputErrorComponent,
    DescriberWithActionComponent,
    ActionLabelComponent,
    CollapseBookmarkComponent,
    CardComponent,
    ConversionMiniStatisticComponent,
    MinistatisticComponent,
    ReferrerStatisticComponent,
    ImageComponent,
    PercentsComponent,
    InputSearchComponent,
    ConversionTooltipInfoComponent,
    TogglerComponent,
    CVTooltipContainerComponent,
    ProjectSettingsComponent,
    InstallCodeComponent,
    SearchListComponent,
    SelectComponent,
    TrackingCodeComponent,
    TeamMembersComponent,
    SourceFaviconComponent,
    // pipes
    SignedPercentPipe,
    FromNowPipe,
    PeriodPricePipe,
    RoundPipe,
    // directives
    EllipsisDirective
  ]
})
export class SharedModule { }

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { of as locOf, Observable, throwError, Subject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { template } from 'lodash';
import { apiMap } from '../maps/api.map';
import { API_CFG_DEFAULTS } from '../maps/defaults.map';
import { ApiCfg } from '../interfaces/args.interface';
import { NotificationService } from './notification.service';

type ResponseType = 'text' | 'json' | 'blob' | 'arraybuffer';

@Injectable()
export class DataService {
  private _apiMap = apiMap;
  private _defaultHeaders = { 'Content-Type': 'application/json' };

  constructor(
    private _http: HttpClient,
    private notificationService: NotificationService
  ) { }

  api(cfg: ApiCfg) {
    cfg = Object.assign({}, API_CFG_DEFAULTS, cfg);
    const apiMapItem = this._apiMap[cfg.type];
    if (!apiMapItem)
      throw "no such api type in apiMap: " + cfg.type;
    if (apiMapItem.mockup)
      return locOf(apiMapItem.mockup);
    if (apiMapItem.errMockup) {
      return throwError(apiMapItem.errMockup);
    }

    const headers = Object.assign(
      {},
      this._defaultHeaders,
      (apiMapItem.headers || {}),
      (cfg.headers || {})
    );
    const params = this.getParams(cfg.queryParams);
    const url = this.getUrl(apiMapItem.url + params, cfg);

    const response$ = this._http.request(apiMapItem.method, url, {
      body: cfg.data,
      headers: new HttpHeaders(headers),
      observe: 'response',
      responseType: <ResponseType>(cfg.responseType || apiMapItem.responseType || 'text'),
      withCredentials: false
    });

    return this.responseHandler(response$, cfg);
  }

  /**
 * Adds default headers that will appear on every HTTP request
 */
  addHeaders(headers: any) {
    Object.assign(this._defaultHeaders, headers);
  }

  /**
   * Removes default headers that will appear on every HTTP request
   */
  remHeaders(keys: string[]) {
    keys.forEach(key => delete this._defaultHeaders[key]);
  }

  /**
 * returns an encoded param string
 */
  private getParams(queryParams: any): string {
    if (!queryParams) return '';
    let params = '';

    for (let paramKey in queryParams) {
      let qParam = queryParams[paramKey];

      if (qParam === null || typeof qParam === 'undefined') continue;

      qParam = encodeURIComponent(qParam);
      params += paramKey + '=' + qParam + '&';
    }
    params = '?' + params.substr(0, params.length - 1);
    return params;
  }

  getUrl(urlTemplate: string, cfg: ApiCfg) {
    if (!cfg.urlParams) {
      cfg.urlParams = {};
    }
    return template(urlTemplate)(cfg);
  }

  private responseHandler(res: Observable<any>, cfg: ApiCfg) {
    return res.pipe(
      map(res => {
        try {
          // This will fail if the response is a string.
          return JSON.parse(res.body);
        } catch (err) {
          return res.body;
        }
      }),
      catchError(err => {        
        if (err.status == 401) {
          this.notificationService.sendAuthMessage('UNAUTHORIZED');
        }
        return throwError(err)
      })
    );
  }

  private subject = new Subject<any>();

  sendMessage(val) {
    this.subject.next(val);
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }
}

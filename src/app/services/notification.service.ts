import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private _authNotify = new Subject<any>();
  private _siteChangeNotify = new Subject<any>();

  constructor() {

  }

  sendAuthMessage(value): void {
    this._authNotify.next(value);
  }

  getAuthMessage(): Observable<any> {
    return this._authNotify.asObservable();
  }

  sendSiteChangeMessage(value): void {
    this._siteChangeNotify.next(value);
  }

  getSiteChangeMessage(): Observable<any> {
    return this._siteChangeNotify.asObservable();
  }


}

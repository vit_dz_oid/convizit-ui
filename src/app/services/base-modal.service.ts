import { ComponentFactoryResolver, Injector } from '@angular/core';
import { DialogService } from '../vendor/ng2-bootstrap-modal/dialog.service';
import { Observable } from 'rxjs';
import { cloneDeep } from 'lodash';

import { ModalCfg } from '../interfaces/modalCfg.interface';

export class BaseModalService {
    constructor(private cfg: {
        modalMap: any,
        ModalWrapperComponent: any,
        dialogService: DialogService,
        resolver: ComponentFactoryResolver,
        injector: Injector,
    }) { }

    /**
     * Opens a modal whose properties are defined by the modalMap. The specific
     * modal will be injected into the ModalWrapperComponent and will inherit all
     * its methods by calling super(injector) in its constructor.
     */
    set(modalCfg: ModalCfg): Observable<any> {
        let specificModal = cloneDeep(this.cfg.modalMap[modalCfg.type]);

        if (!specificModal.options) specificModal.options = {};
        if (!specificModal.args) specificModal.args = {};
        if (!specificModal.dialogOptions) specificModal.dialogOptions = {};

        for (let key in modalCfg.options) {
            specificModal.options[key] = modalCfg.options[key];
        }

        for (let key in modalCfg.args) {
            specificModal.args[key] = modalCfg.args[key];
        }

        if (modalCfg.btns) specificModal.btns = modalCfg.btns;

        specificModal.title = modalCfg.title || specificModal.title;

        const componentClass = this.getComponentClassFromSelector(this.cfg.resolver, specificModal.selector);
        specificModal.factory = this.cfg.resolver.resolveComponentFactory(componentClass);
        specificModal.injector = this.cfg.injector;


        return this.cfg.dialogService.addDialog(
            this.cfg.ModalWrapperComponent,
            specificModal.options || {},
            { componentData: specificModal },
            specificModal.dialogOptions
        );
    }

    getLayerIdx() {
        let dialogs = document.getElementsByTagName('dialog-wrapper');
        return dialogs.length;
    }

    /**
     * Retrieves the component class via its selector string.
     * @param componentFactoryResolver instanceof {ComponentFactoryResolver} from @angular/core
     * @param selector {String} the component's selector
     */
    private getComponentClassFromSelector(componentFactoryResolver, selector) {
        let result;
        componentFactoryResolver._factories.forEach((val, key) => {
            if (val.selector === selector) {
                result = key;
            }
        });
        if (!result) {
            result = this.getComponentClassFromSelector(componentFactoryResolver._parent, selector);
        }
        return result;
    }
}

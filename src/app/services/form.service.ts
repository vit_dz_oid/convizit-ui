import { Injectable } from '@angular/core';
import { FormControl, FormGroup, FormArray, AbstractControl } from '@angular/forms';

@Injectable()
export class FormService {
  private _emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  constructor() { }

  validators = {
    emailPattern: (ctrl: FormControl) => (ctrl.value && !ctrl.value.match(this._emailPattern)) ? { emailPattern: true } : null,
  }
  /**
   * 
   * @param control 
   * @param errors - "output" array of errors : [{ name: erorName}]
   */
  errorGrabber(control: AbstractControl, errors = []) {
    const _errors = []
    if (control.errors) {
      this.errorsTranspile(control.errors).forEach(err => errors.push(err));
      _errors.concat(this.errorsTranspile(control.errors));
    }

    if (control instanceof FormGroup) {
      Object.keys(control.controls).forEach((controlName) => {
        this.errorGrabber(control.controls[controlName], errors);
        _errors.concat(this.errorsTranspile(control.controls[controlName]));
      });
      
    } else if (control instanceof FormArray) {
      control.controls.forEach(ctrl => this.errorGrabber(ctrl, errors));
    } else {
      return ;
    }
  }

  errorsTranspile(errors) {
    const result = Object.keys(errors).map(name => ({ name }));
    return result;
  }
}

import { Injectable } from '@angular/core';
import { template } from 'lodash';
import { messages } from '../maps/messges.map';

@Injectable()
export class MessageService {

  constructor() { }

  getInputErrorMessage(type: string, params?: any) {
    const message = messages.errors.input[type];

    if(message && message.match(/<%=(.*)%>/)) {
      return template(message)(params);
    }

    return message;
  }
}

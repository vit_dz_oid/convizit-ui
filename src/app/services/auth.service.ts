import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { DataService } from './data.service';
import { LoginStatus } from '../interfaces/server.interface';
import { LoginResponse, CheckTokenResponse, EmailUsedResponse } from '../interfaces/auth.interface';
import { NotificationService } from './notification.service';

const DEFAULT_LOGIN_STATUS = {
  isLogin: false,
  isVerified: false,
  isActive: false,
  trialDays: false,
  isOwner: false,
};
@Injectable()
export class AuthService {

  private _loggedStatus$ = new BehaviorSubject<LoginStatus>(DEFAULT_LOGIN_STATUS);

  loggedStatus: Promise<LoginStatus>;

  get status() {
    return this._loggedStatus$.getValue();
  }

  constructor(
    private dataService: DataService,
    private router: Router,
    private notificationService: NotificationService
  ) {
    this.notificationService.getAuthMessage()
      .subscribe(this._messageProcessing);

    this.notificationService.getSiteChangeMessage()
      .subscribe(data => {
        this.isLogged()
          .subscribe(() => {
            this.router.navigateByUrl('');
          });
      })

    this.loggedStatus = this.isLogged().toPromise();
  }

  isLogged() {
    return this.dataService.api({
      type: 'isLogged',
    }).pipe(
      tap(res => this._loggedStatus$.next(res))
    );
  }

  login(logindata: { username: string, password: string }): Promise<LoginResponse> {
    return new Promise<LoginResponse>((resolve, reject) => {
      this.dataService.api({
        type: 'login',
        data: Object.assign({}, logindata)
      }).subscribe(
        res => {
          if (!res.error) {
            this.loggedStatus = this.isLogged().toPromise();
            resolve(res);
          } else {
            reject(res.error);
          }
        },
        err => {
          reject(err);
        });
    });
  }

  resetPassword(resetData: { email: string }): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      this.dataService.api({
        type: 'resetPassword',
        data: Object.assign({}, resetData)
      }).subscribe(
        res => {
          if (!res.error) {
            resolve(res);
          } else {
            reject(res.error);
          }
        },
        err => {
          reject(err);
        })
    })
  }

  checkResetToken(resetData: { token: string }): Promise<CheckTokenResponse> {
    return new Promise<CheckTokenResponse>((resolve, reject) => {
      this.dataService.api({
        type: 'checkResetToken',
        data: Object.assign({}, resetData)
      }).subscribe(
        res => {
          if (!res.error) {
            resolve(res);
          } else {
            reject(res.error);
          }
        },
        err => {
          reject(err);
        })
    })
  }

  setPassword(setData: { password: string, token: string }): Promise<number> {
    return new Promise<number>((resolve, reject) => {
      this.dataService.api({
        type: 'setPassword',
        data: Object.assign({}, setData)
      }).subscribe(
        res => {
          if (!res.error) {
            resolve(res);
          } else {
            reject(res.error);
          }
        },
        err => {
          reject(err);
        })
    })
  }

  checkEmailUsed(data: { email: string }): Promise<EmailUsedResponse> {
    return new Promise<EmailUsedResponse>((resolve, reject) => {
      this.dataService.api({
        type: 'checkEmailUsed',
        data: Object.assign({}, data)
      }).subscribe(
        res => {
          if (!res.error) {
            resolve(res);
          } else {
            reject(res.error);
          }
        },
        err => {
          reject(err);
        })
    });
  }

  registration(registerData: { name: string, username: string, password: string, company: string, size: string, title: string }): Promise<LoginResponse> {
    return new Promise<LoginResponse>((resolve, reject) => {
      this.dataService.api({
        type: 'signup',
        data: Object.assign({}, registerData)
      }).subscribe(
        res => {
          if (!res.error) {
            this.loggedStatus = this.isLogged().toPromise();
            resolve(res);
          } else {
            reject(res.error);
          }
        },
        err => {
          reject(err);
        })
    });
  }

  private _messageProcessing = (val: any) => {

    if (typeof val === 'string') {
      console.log(val);

      // switch (val) {
      //   case 'UNAUTHORIZED':
      //     this.loggedStatus = Promise.resolve(DEFAULT_LOGIN_STATUS);
      //     this.router.navigate(['/auth']);
      //   default:

      // }
    }
  }
}

import { Injectable, ComponentFactoryResolver, Injector } from '@angular/core';
import { DialogService } from '../vendor/ng2-bootstrap-modal/dialog.service';
import { Observable } from 'rxjs';

import { BaseModalService } from './base-modal.service';
import { ModalWrapperComponent } from '../modals/modal-wrapper.component';
import { modalMap } from '../maps/modal.map';

@Injectable()
export class ModalService extends BaseModalService {
    constructor(
        dialogService: DialogService,
        resolver: ComponentFactoryResolver,
        injector: Injector,
    ) {
        super({ modalMap, ModalWrapperComponent, dialogService, resolver, injector });
    }

    /**
     * Opens a notification popup with an 'ok' button
     */
    notification(cfg: { body?: string, bodyKey?: string }): Observable<any> {
        let body;
        if (cfg.bodyKey) {
            body = cfg.bodyKey;
        } else if (cfg.body) {
            body = cfg.body;
        }

        return this.set({
            type: 'notification',
            args: {
                iconType: 'notification-icon',
                body: body,
                enterToClose: 'ok'
            }
        });
    }

    /**
     * Opens a error popup with an 'ok' button
     */
    error(cfg: { body?: string, bodyKey?: string }): Observable<any> {
        let body;
        if (cfg.bodyKey) {
            body = cfg.bodyKey;
        } else if (cfg.body) {
            body = cfg.body;
        }

        return this.set({
            type: 'error',
            args: {
                iconType: 'error_popup',
                body: body,
                enterToClose: 'ok'
            }
        });
    }

    /**
     * Opens a error popup with a 'close' and 'continue' buttons
     */
    warning(cfg: { body?: string, bodyKey?: string }): Observable<any> {
        let body;
        if (cfg.bodyKey) {
            body = cfg.bodyKey;
        } else if (cfg.body) {
            body = cfg.body;
        }

        return this.set({
            type: 'warning',
            args: {
                iconType: 'warning_popup',
                body
            }
        });
    }

    /**
     * Opens a error popup with a 'cancel' and 'continue' buttons
     */
    confirmation(cfg: { body?: string, bodyKey?: string }): Observable<any> {
        let body;
        if (cfg.bodyKey) {
            body = cfg.bodyKey;
        } else if (cfg.body) {
            body = cfg.body;
        }

        return this.set({
            type: 'confirmation',
            args: {
                iconType: 'confirmation_popup',
                body: body
            }
        });
    }
}

import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'convizit-app',
  template: `<router-outlet></router-outlet>`
})
export class AppComponent {
}

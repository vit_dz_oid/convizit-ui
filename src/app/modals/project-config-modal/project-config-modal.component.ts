import { Component, OnInit, Injector } from '@angular/core';
import { ModalBaseComponent } from '../modal-base-component.component';

@Component({
  selector: 'cv-project-config-modal',
  templateUrl: './project-config-modal.component.html',
  styleUrls: ['./project-config-modal.component.scss']
})
export class ProjectConfigModalComponent extends ModalBaseComponent implements OnInit {
  projectId = null;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {    
    this.projectId = this.args.projectId;
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectConfigModalComponent } from './project-config-modal.component';

describe('ProjectConfigModalComponent', () => {
  let component: ProjectConfigModalComponent;
  let fixture: ComponentFixture<ProjectConfigModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectConfigModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectConfigModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

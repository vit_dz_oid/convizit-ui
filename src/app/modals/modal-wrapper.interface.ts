import { ComponentFactory, Injector } from '@angular/core';

export interface ComponentData {
    selector: string;  // component selector
    factory: ComponentFactory<any>;  // component-factory that will be inserted in #modalContent.
    injector: Injector;  // the injector that will be passed-on to the specific modal.
    title?: string;  // the modal title
    btns: any[];  // an array of modal-buttons, that will be in the modal-footer
    args: any;  // the args are received from the modalService.set({args: {}}) function caller)
}

import { Component, OnInit, Injector } from '@angular/core';
import { ModalBaseComponent } from '../modal-base-component.component';

@Component({
  selector: 'cv-install-code-modal',
  templateUrl: './install-code-modal.component.html',
  styleUrls: ['./install-code-modal.component.scss']
})
export class InstallCodeModalComponent extends ModalBaseComponent implements OnInit {

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
  }

}

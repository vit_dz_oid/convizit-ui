import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstallCodeModalComponent } from './install-code-modal.component';

describe('InstallCodeModalComponent', () => {
  let component: InstallCodeModalComponent;
  let fixture: ComponentFixture<InstallCodeModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstallCodeModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstallCodeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

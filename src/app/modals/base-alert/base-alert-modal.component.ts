import { Component, Injector, HostListener } from '@angular/core';

import { ModalBaseComponent } from '../modal-base-component.component';

@Component({
    selector: 'cv-base-alert-modal',
    templateUrl: './base-alert-modal.component.html',
    styleUrls: ['./base-alert-modal.component.scss']
})

export class CVBaseAlertModalComponent extends ModalBaseComponent {
    constructor(injector: Injector) {
        super(injector);
    }

    ngOnInit() {
        this.registerBtn('ok', () => this.modalConfirm('ok'));
    }

    @HostListener('body:keyup.enter', ['$event'])
    closeOnEnter(event) {
        if (this.args.enterToClose) {
            event.preventDefault();
            this.modalConfirm(this.args.enterToClose);
        }
    }
}

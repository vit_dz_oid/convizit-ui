import { Component, Input, ReflectiveInjector, ViewChild, ViewContainerRef } from '@angular/core';
import { DialogComponent } from '../vendor/ng2-bootstrap-modal/dialog.component';
import { DialogService } from '../vendor/ng2-bootstrap-modal/dialog.service';

import { ComponentData } from './modal-wrapper.interface';

@Component({
    selector: 'cv-modal-wrapper',
    templateUrl: './modal-wrapper.component.html',
})
export class ModalWrapperComponent extends DialogComponent<any, any> {
    @Input() set componentData(data: ComponentData) {
        if (!data) return;
        this.title = data.title;
        this.btns = data.btns;
        const resolvedArgs = ReflectiveInjector.resolve([
            { provide: 'registerBtn', useValue: this.registerBtn.bind(this) },
            { provide: 'disableBtn', useValue: this.disableBtn.bind(this) },
            { provide: 'modalConfirm', useValue: this.modalConfirm.bind(this) },
            { provide: 'modalClose', useValue: this.close.bind(this) },
            { provide: 'args', useValue: data.args || {} },
            { provide: 'labelBtn', useValue: this.labelBtn.bind(this) }
        ]);
        const injector = ReflectiveInjector.fromResolvedProviders(resolvedArgs, data.injector);
        const componentInstance = data.factory.create(injector);
        this.modalContent.insert(componentInstance.hostView);
        if (this.currentComponent) this.currentComponent.destroy();
        this.currentComponent = componentInstance;
    }



    @ViewChild('modalContent', { read: ViewContainerRef }) modalContent: ViewContainerRef;

    currentComponent = null;
    title: string;
    btns: any[];
    btnFncsMap = {};
    disableBtnFncsMap = {};

    constructor(dialogService: DialogService) {
        super(dialogService);
    }

    /**
     * Closes the modal with a result. This value will be received by the
     * modalService.set().subscribe() after the modal closes.
     */
    modalConfirm(result) {
        this.result = result;
        this.close();
    }

    /**
     * Registers a function that will be called when the specific button
     * is pressed (by button-id).
     */
    registerBtn(btnId, fnc) {
        this.btnFncsMap[btnId] = fnc;
    }

    /**
     * Registers a function (that returns a boolean) that will decide whether
     * a specific button is disabled (by button-id).
     */
    disableBtn(btnId, fnc) {
        this.disableBtnFncsMap[btnId] = fnc;
    }

    /**
     * Will be executed whenever a button (in modal-footer) is pressed.
     * Checks if a function was registered to this button. If not, will
     * close the modal.
     */
    btnClicked(btnId) {
        if (this.btnFncsMap[btnId]) this.btnFncsMap[btnId]();
        else this.close();
    }

    /**
     * Will be executed whenever a button (in modal-footer) is pressed.
     * Checks if a disable-function was registered for this button.
     * If not, button will always be enabled.
     */
    btnDisable(btnId) {
        return this.disableBtnFncsMap[btnId] ? this.disableBtnFncsMap[btnId]() : false;
    }

    labelBtn(btnId, label) {
        const btn = this.btns.find(b => b.id === btnId);
        if (btn) btn.label = label;
    }
}

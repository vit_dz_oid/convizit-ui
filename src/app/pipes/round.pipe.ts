import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'round'
})
export class RoundPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    if (!value) {
      return 0;
    }
    if (!args) {
      return Math.round(value);
    }
    if (Math.abs(value) < Math.pow(10, -args)) { // otherwise, 0.01 -> 0.0
      return 0;
    }

    value += '';
    const split = value.split('.');

    let res = split[0];

    if (split[1]) {
      res += '.' + split[1].slice(0, args);
    }

    return res;
  }

}

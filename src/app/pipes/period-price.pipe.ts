import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'periodPrice'
})
export class PeriodPricePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let isMonthly = args;

    if (isMonthly) return value;
    else {
      var discount = Math.round(value * 12 * 0.15);
      return value * 12 - discount;
    }
  }

}

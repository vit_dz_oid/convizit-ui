import { Pipe, PipeTransform } from '@angular/core';
import { PercentPipe } from '@angular/common';

@Pipe({
  name: 'signpercent',
})
export class SignedPercentPipe implements PipeTransform {
  constructor(
    private percentPipe: PercentPipe
  ) {}

  transform(value: any, symb?: string): any { 
    return `${value === 0 ? '': (value < 0 ? '': '+')}${this.percentPipe.transform(value, symb || '.0-2')}`.split(',').join('');
  }
}

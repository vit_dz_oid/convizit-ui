import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
    name: 'fromNow'
})
export class FromNowPipe implements PipeTransform {

    transform(value: any, args?: string): any {
        const [bucketSpan = 0, prefix] = args.toString().split(':');

        //TODO: refactoring
        let datePrefix = '';

        if (prefix) {
            if (bucketSpan < 86400) {
                datePrefix = 'At ';
            } else if (bucketSpan >= 86400 && bucketSpan < 604800) {
                datePrefix = 'On ';
            } else if (bucketSpan >= 604800) {
                datePrefix = 'In ';
            }
        }

        if (bucketSpan < 3600) {

            let textPrefix = '';

            const a = moment();
            const b = moment(value * 1000);
            const diff = a.diff(b, 'days');
            if (diff == 0) {
                textPrefix = 'Today';
            } else if (diff == 1) {
                textPrefix = 'Yesterday';
            } else if (diff == 2) {
                textPrefix = '2 days ago';
            } else if (diff == 3) {
                textPrefix = '3 days ago';
            } else {
                textPrefix = `On ${b.format('MMMM D')}`
            }

            return `${textPrefix}, ${datePrefix.toLowerCase()}${moment(value * 1000).format('LT')}`;

        } else if (bucketSpan >= 3600 && bucketSpan < 86400) {

            let textPrefix = '';

            const a = moment();
            const b = moment(value * 1000);
            const diff = a.diff(b, 'days');
            if (diff == 0) {
                textPrefix = 'Today';
            } else if (diff == 1) {
                textPrefix = 'Yesterday';
            } else if (diff == 2) {
                textPrefix = '2 days ago';
            } else if (diff == 3) {
                textPrefix = '3 days ago';
            } else {
                textPrefix = `On ${b.format('MMMM D')}`
            }

            return `${textPrefix}, ${datePrefix}${moment(value * 1000).format('h A')}`;

        } else if (bucketSpan >= 86400 && bucketSpan < 604800) {
            const a = moment();
            const b = moment(value * 1000);
            const diff = a.diff(b, 'days');
            if (diff == 0) {
                return 'Today';
            } else if (diff == 1) {
                return 'Yesterday';
            } else if (diff == 2) {
                return '2 days ago';
            } else if (diff == 3) {
                return '3 days ago';
            } else {
                return `${datePrefix}${b.format('MMMM D')}`
            }
        } else if (bucketSpan >= 604800 && bucketSpan < 2592000) {

            const a = moment();
            const b = moment(value * 1000);
            const diff = a.diff(b, 'week');
            if (diff == 0) {
                return 'This week';
            } else if (diff == 1) {
                return 'Last week';
            } else if (diff == 2) {
                return '2 weeks ago';
            } else if (diff == 3) {
                return '3 weeks ago';
            } else {
                return `${datePrefix}the week of ${b.format('MMMM D')}`
            }

        } else if (bucketSpan >= 2592000) {

            const a = moment();
            const b = moment(value * 1000);
            const diff = a.diff(b, 'month');

            if (diff == 0) {
                return 'This month';
            } else if (diff == 1) {
                return 'Last month';
            } else {
                return `${datePrefix}${b.format('MMMM YYYY')}`
            }

        }
    }
}

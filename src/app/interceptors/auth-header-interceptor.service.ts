import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AUTH_HEADER } from '../maps/config.map';

@Injectable()
export class AuthHeaderInterceptorService implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authHeader = null;// will get some auth header data 

        if (authHeader) {
            const authReq = req.clone({
                setHeaders: { [AUTH_HEADER]: authHeader }
            });
            return next.handle(authReq);
        } else {
            return next.handle(req);
        }
  }
}

import { TestBed, async, inject } from '@angular/core/testing';

import { IsVerifiedGuard } from './is-verified.guard';

describe('IsVerifiedGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsVerifiedGuard]
    });
  });

  it('should ...', inject([IsVerifiedGuard], (guard: IsVerifiedGuard) => {
    expect(guard).toBeTruthy();
  }));
});

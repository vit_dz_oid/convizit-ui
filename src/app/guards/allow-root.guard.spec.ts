import { TestBed, async, inject } from '@angular/core/testing';

import { AllowRootGuard } from './allow-root.guard';

describe('AllowRootGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AllowRootGuard]
    });
  });

  it('should ...', inject([AllowRootGuard], (guard: AllowRootGuard) => {
    expect(guard).toBeTruthy();
  }));
});

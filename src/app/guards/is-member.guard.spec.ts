import { TestBed, async, inject } from '@angular/core/testing';

import { IsMemberGuard } from './is-member.guard';

describe('IsMemberGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsMemberGuard]
    });
  });

  it('should ...', inject([IsMemberGuard], (guard: IsMemberGuard) => {
    expect(guard).toBeTruthy();
  }));
});

import { TestBed, async, inject } from '@angular/core/testing';

import { IsNotVerifiedGuard } from './is-not-verified.guard';

describe('IsNotVerifiedGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsNotVerifiedGuard]
    });
  });

  it('should ...', inject([IsNotVerifiedGuard], (guard: IsNotVerifiedGuard) => {
    expect(guard).toBeTruthy();
  }));
});

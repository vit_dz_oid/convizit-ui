import { TestBed, async, inject } from '@angular/core/testing';

import { IsNotActiveGuard } from './is-not-active.guard';

describe('IsNotActiveGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsNotActiveGuard]
    });
  });

  it('should ...', inject([IsNotActiveGuard], (guard: IsNotActiveGuard) => {
    expect(guard).toBeTruthy();
  }));
});

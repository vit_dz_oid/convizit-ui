import { TestBed, async, inject } from '@angular/core/testing';

import { AllowAuthGuard } from './allow-auth.guard';

describe('AllowAuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AllowAuthGuard]
    });
  });

  it('should ...', inject([AllowAuthGuard], (guard: AllowAuthGuard) => {
    expect(guard).toBeTruthy();
  }));
});

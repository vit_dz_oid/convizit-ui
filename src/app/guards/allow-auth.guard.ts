import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { LoginStatus } from '../interfaces/server.interface';

@Injectable({
  providedIn: 'root'
})
export class AllowAuthGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    return this.authService.loggedStatus
      .then((res: LoginStatus) => {
        const { isLogin } = res;

        if (isLogin) {
          this.router.navigateByUrl('');
          return false;
        } else {
          return true;
        }

      });

  }
}

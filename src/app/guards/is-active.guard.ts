import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class IsActiveGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    const { isActive, isOwner } = this.authService.status;

    if (!isActive) {
      if (isOwner) {
        this.router.navigateByUrl('pricing');
      } else {
        this.router.navigateByUrl('inactive');
      }
      return false;
    }

    return true;

  }
}

import { TestBed, async, inject } from '@angular/core/testing';

import { IsNotActivatedGuard } from './is-not-activated.guard';

describe('IsNotActivatedGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsNotActivatedGuard]
    });
  });

  it('should ...', inject([IsNotActivatedGuard], (guard: IsNotActivatedGuard) => {
    expect(guard).toBeTruthy();
  }));
});

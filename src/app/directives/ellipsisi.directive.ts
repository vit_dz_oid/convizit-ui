import { Directive, HostListener, ElementRef, Renderer2, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[cvEllipsis]'
})
export class EllipsisDirective implements AfterViewInit {

  constructor(private el: ElementRef, private renderer: Renderer2) { }

  ngAfterViewInit() {
    this.renderer.addClass(this.el.nativeElement, 'ellipsis-overflow');
  }

  @HostListener('mouseover')
  onMouseOver() {
    if (this.el.nativeElement.scrollWidth > this.el.nativeElement.offsetWidth) {
      this.renderer.setAttribute(this.el.nativeElement, 'title', this.el.nativeElement.innerText.trim());
    } else {
      this.renderer.setAttribute(this.el.nativeElement, 'title', '');
    }
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { environment } from '../environments/environment';
import { AllowAuthGuard } from './guards/allow-auth.guard';
import { AllowRootGuard } from './guards/allow-root.guard';

const routes: Routes = [
    {
        path: 'auth',
        loadChildren: './modules/auth/auth.module#AuthModule',
        canActivate: [AllowAuthGuard]
    },
    {
        path: '',
        loadChildren: './modules/workstage/workstage.module#WorkstageModule',
        canActivate: [AllowRootGuard]
    },
    { path: '**', redirectTo: '' },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            enableTracing: false,//!environment.production // <-- tracing of navigation
            onSameUrlNavigation: 'reload'
        }),
    ],
    providers: [],
    exports: [
        RouterModule,
    ]
})
export class AppRouting { }
